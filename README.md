# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* BAF Portal
* Version 1.0

[![buddy pipeline](https://app.buddy.works/buluma/baf-portal/pipelines/pipeline/62819/badge.svg?token=c0c0ea17b4e860d108f2a66057e9dd7d3a49779338d0ece69f674cc05d74c276 "buddy pipeline")](https://app.buddy.works/buluma/baf-portal/pipelines/pipeline/62819)

[![Build status](https://ci.appveyor.com/api/projects/status/m21l6vi9127r7iu8/branch/master?svg=true)](https://ci.appveyor.com/project/buluma_michael/baf-portal/branch/master)
### How do I get set up? ###

* Clone Repo
* Work on local branch
* export full database with drop table option selected
* upload db in db_dump as filename.sql (you may want to delete the previous filename.sql file)
* sync repo with bitbucket
* confirm site is working correctly: http://gbc.me.ke/dialogkenya/

### Who do I talk to? ###

* Michael Buluma
* Paul Otuoma