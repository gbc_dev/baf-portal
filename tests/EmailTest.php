<?php declare(strict_types=1); use PHPUnit\Framework\TestCase; /**
 * @covers Email
 */ final class EmailTest extends TestCase {
    public function testCanBeCreatedFromValidEmailAddress(): void
    {
        $this->assertInstanceOf(
            Email::class,
            Email::fromString('ops@buluma.me.ke')
        );
    }
    public function testCannotBeCreatedFromInvalidEmailAddress(): void
    {
        $this->expectException(InvalidArgumentException::class);
        Email::fromString('invalid');
    }
    public function testCanBeUsedAsString(): void
    {
        $this->assertEquals(
            'ops@buluma.me.ke',
            Email::fromString('ops@buluma.me.ke')
        );
    }
}
