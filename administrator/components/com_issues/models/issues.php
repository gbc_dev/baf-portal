<?php

/**
 * @version    CVS: 2.0.1
 * @package    Com_Issues
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Issues records.
 *
 * @since  1.6
 */
class IssuesModelIssues extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
				'modified_by', 'a.`modified_by`',
				'issue_title', 'a.`issue_title`',
				'category', 'a.`category`',
				'description', 'a.`description`',
				'attachments', 'a.`attachments`',
				'attachment_found', 'a.`attachment_found`',
				'file_upload', 'a.`file_upload`',
				'manuals', 'a.`manuals`',
				'research', 'a.`research`',
				'first_key_player', 'a.`first_key_player`',
				'issue_players', 'a.`issue_players`',
				'published', 'a.`published`',
				'created', 'a.`created`',
				'modified', 'a.`modified`',
				'publish_up', 'a.`publish_up`',
				'publish_down', 'a.`publish_down`',
				'access', 'a.`access`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering category
		$this->setState('filter.category', $app->getUserStateFromRequest($this->context.'.filter.category', 'filter_category', '', 'string'));

		// Filtering first_key_player
		$this->setState('filter.first_key_player', $app->getUserStateFromRequest($this->context.'.filter.first_key_player', 'filter_first_key_player', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_issues');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.issue_title', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__issues` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Join over the user field 'modified_by'
		$query->select('`modified_by`.name AS `modified_by`');
		$query->join('LEFT', '#__users AS `modified_by` ON `modified_by`.id = a.`modified_by`');

		// Join over the access level field 'access'
		$query->select('`access`.title AS `access`');
		$query->join('LEFT', '#__viewlevels AS access ON `access`.id = a.`access`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.issue_title LIKE ' . $search . '  OR  a.category LIKE ' . $search . '  OR  a.description LIKE ' . $search . ' )');
			}
		}


		// Filtering category
		$filter_category = $this->state->get("filter.category");

		if ($filter_category !== null && !empty($filter_category))
		{
			$query->where("a.`category` = '".$db->escape($filter_category)."'");
		}

		// Filtering first_key_player
		$filter_first_key_player = $this->state->get("filter.first_key_player");

		if ($filter_first_key_player !== null && (is_numeric($filter_first_key_player) || !empty($filter_first_key_player)))
		{
			$query->where('FIND_IN_SET(' . $db->quote($filter_first_key_player) . ', ' . $db->quoteName('a.first_key_player') . ')');
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		foreach ($items as $oneItem)
		{

			if (isset($oneItem->category))
			{
				$db    = JFactory::getDbo();
				$query = $db->getQuery(true);

				$query
					->select($db->quoteName('title'))
					->from($db->quoteName('#__categories'))
					->where('FIND_IN_SET(' . $db->quoteName('id') . ', ' . $db->quote($oneItem->category) . ')');

				$db->setQuery($query);
				$result = $db->loadColumn();

				$oneItem->category = !empty($result) ? implode(', ', $result) : '';
			}

			if (isset($oneItem->manuals))
			{
				$values    = explode(',', $oneItem->manuals);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "SELECT id, file_name, document_name FROM #__manuals_factsheets HAVING id LIKE '" . $value . "'";
						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->file_name;
						}
					}
				}

				$oneItem->manuals = !empty($textValue) ? implode(', ', $textValue) : $oneItem->manuals;
			}

			if (isset($oneItem->research))
			{
				$values    = explode(',', $oneItem->research);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "SELECT id, title FROM #__research_publications HAVING id LIKE '" . $value . "'";
						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->title;
						}
					}
				}

				$oneItem->research = !empty($textValue) ? implode(', ', $textValue) : $oneItem->research;
			}

			if (isset($oneItem->first_key_player))
			{
				$values    = explode(',', $oneItem->first_key_player);
				$textValue = array();

				foreach ($values as $value)
				{
					if (!empty($value))
					{
						$db = JFactory::getDbo();
						$query = "SELECT id, nid FROM #__sobipro_object HAVING id LIKE '" . $value . "'";
						$db->setQuery($query);
						$results = $db->loadObject();

						if ($results)
						{
							$textValue[] = $results->nid;
						}
					}
				}

				$oneItem->first_key_player = !empty($textValue) ? implode(', ', $textValue) : $oneItem->first_key_player;
			}
		}

		return $items;
	}
}
