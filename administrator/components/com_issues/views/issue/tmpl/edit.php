<?php
/**
 * @version    CVS: 2.0.1
 * @package    Com_Issues
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_issues/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.category').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('categoryhidden')){
			js('#jform_category option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_category").trigger("liszt:updated");
	js('input:hidden.manuals').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('manualshidden')){
			js('#jform_manuals option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_manuals").trigger("liszt:updated");
	js('input:hidden.research').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('researchhidden')){
			js('#jform_research option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_research").trigger("liszt:updated");
	js('input:hidden.first_key_player').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('first_key_playerhidden')){
			js('#jform_first_key_player option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_first_key_player").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'issue.cancel') {
			Joomla.submitform(task, document.getElementById('issue-form'));
		}
		else {
			
			if (task != 'issue.cancel' && document.formvalidator.isValid(document.id('issue-form'))) {
				
	if(js('#jform_category option:selected').length == 0){
		js("#jform_category option[value=0]").attr('selected','selected');
	}
	if(js('#jform_manuals option:selected').length == 0){
		js("#jform_manuals option[value=0]").attr('selected','selected');
	}
	if(js('#jform_research option:selected').length == 0){
		js("#jform_research option[value=0]").attr('selected','selected');
	}
	if(js('#jform_first_key_player option:selected').length == 0){
		js("#jform_first_key_player option[value=0]").attr('selected','selected');
	}
				Joomla.submitform(task, document.getElementById('issue-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_issues&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="issue-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_ISSUES_TITLE_ISSUE', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php echo $this->form->renderField('created_by'); ?>
				<?php echo $this->form->renderField('modified_by'); ?>				
				<?php echo $this->form->renderField('issue_title'); ?>
				<?php echo $this->form->renderField('category'); ?>

			<?php
				foreach((array)$this->item->category as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="category" name="jform[categoryhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				

				<?php echo $this->form->renderField('description'); ?>
				<?php //echo $this->form->renderField('attachments'); ?>
				<?php echo $this->form->renderField('attachment_found'); ?>
				<?php echo $this->form->renderField('file_name'); ?>
				<?php echo $this->form->renderField('file_upload'); ?>


				<?php if (!empty($this->item->file_upload)) : ?>
					<?php $file_uploadFiles = array(); ?>
					<?php foreach ((array)$this->item->file_upload as $fileSingle) : ?>
						<?php if (!is_array($fileSingle)) : ?>
							<a href="<?php echo JRoute::_(JUri::root() . 'docs' . DIRECTORY_SEPARATOR . $fileSingle, false);?>"><?php echo $fileSingle; ?></a> | 
							<?php $file_uploadFiles[] = $fileSingle; ?>
						<?php endif; ?>
					<?php endforeach; ?>
					<input type="hidden" name="jform[file_upload_hidden]" id="jform_file_upload_hidden" value="<?php echo implode(',', $file_uploadFiles); ?>" />
				<?php endif; ?>	


						

				<?php echo $this->form->renderField('manuals'); ?>

			<?php
				foreach((array)$this->item->manuals as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="manuals" name="jform[manualshidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('research'); ?>

			<?php
				foreach((array)$this->item->research as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="research" name="jform[researchhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('first_key_player'); ?>

			<?php
				foreach((array)$this->item->first_key_player as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="first_key_player" name="jform[first_key_playerhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('issue_players'); ?>
				<input type="hidden" name="jform[published]" value="<?php echo $this->item->published; ?>" />
				<input type="hidden" name="jform[created]" value="<?php echo $this->item->created; ?>" />
				<input type="hidden" name="jform[modified]" value="<?php echo $this->item->modified; ?>" />
				


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php if (JFactory::getUser()->authorise('core.admin','issues')) : ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL', true)); ?>
		<?php echo $this->form->getInput('rules'); ?>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
<?php endif; ?>

<!-- Access LEvels -->
<?php if (JFactory::getUser()->authorise('core.admin','issues')) : ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'publishing', JText::_('Publishing and Access', true)); ?>
		<?php echo $this->form->renderField('access'); ?>
		<?php echo $this->form->renderField('publish_up'); ?>
		<?php echo $this->form->renderField('publish_down'); ?>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
<?php endif; ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
