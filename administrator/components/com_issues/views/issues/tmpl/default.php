<?php
/**
 * @version    CVS: 2.0.1
 * @package    Com_Issues
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_issues/assets/css/issues.css');
$document->addStyleSheet(JUri::root() . 'media/com_issues/css/list.css');

$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_issues');
$saveOrder = $listOrder == 'a.`ordering`';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_issues&task=issues.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'issueList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
?>

<form action="<?php echo JRoute::_('index.php?option=com_issues&view=issues'); ?>" method="post"
	  name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>

            <?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>

			<div class="clearfix"></div>
			<table class="table table-striped" id="issueList">
				<thead>
				<tr>
					<?php if (isset($this->items[0]->ordering)): ?>
						<th width="1%" class="nowrap center hidden-phone">
                            <?php echo JHtml::_('searchtools.sort', '', 'a.`ordering`', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING', 'icon-menu-2'); ?>
                        </th>
					<?php endif; ?>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value=""
							   title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
					</th>
					<?php if (isset($this->items[0]->state)): ?>
						<th width="1%" class="nowrap center">
								<?php echo JHtml::_('searchtools.sort', 'JSTATUS', 'a.`state`', $listDirn, $listOrder); ?>
</th>
					<?php endif; ?>

									<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_ID', 'a.`id`', $listDirn, $listOrder); ?>
				</th>
				<th class='left hidden'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_CREATED_BY', 'a.`created_by`', $listDirn, $listOrder); ?>
				</th>
				<th class='left hidden'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_MODIFIED_BY', 'a.`modified_by`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_ISSUE_TITLE', 'a.`issue_title`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_CATEGORY', 'a.`category`', $listDirn, $listOrder); ?>
				</th>
				<th class='left hidden'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_DESCRIPTION', 'a.`description`', $listDirn, $listOrder); ?>
				</th>
				<th class='left hidden'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_ATTACHMENTS', 'a.`attachments`', $listDirn, $listOrder); ?>
				</th>
				<th class='left hidden'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_FILE_UPLOAD', 'a.`file_upload`', $listDirn, $listOrder); ?>
				</th>
				<th class='left hidden'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_MANUALS', 'a.`manuals`', $listDirn, $listOrder); ?>
				</th>
				<th class='left hidden'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_RESEARCH', 'a.`research`', $listDirn, $listOrder); ?>
				</th>
				<th class='left hidden'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_FIRST_KEY_PLAYER', 'a.`first_key_player`', $listDirn, $listOrder); ?>
				</th>
				<th class='left hidden'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_ISSUE_PLAYERS', 'a.`issue_players`', $listDirn, $listOrder); ?>
				</th>
				<th class='left hidden'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_PUBLISHED', 'a.`published`', $listDirn, $listOrder); ?>
				</th>
				<th class='left hidden'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_CREATED', 'a.`created`', $listDirn, $listOrder); ?>
				</th>
				<th class='left hidden'>
				<?php echo JHtml::_('searchtools.sort',  'COM_ISSUES_ISSUES_MODIFIED', 'a.`modified`', $listDirn, $listOrder); ?>
				</th>

					
				</tr>
				</thead>
				<tfoot>
				<tr>
					<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
				</tfoot>
				<tbody>
				<?php foreach ($this->items as $i => $item) :
					$ordering   = ($listOrder == 'a.ordering');
					$canCreate  = $user->authorise('core.create', 'com_issues');
					$canEdit    = $user->authorise('core.edit', 'com_issues');
					$canCheckin = $user->authorise('core.manage', 'com_issues');
					$canChange  = $user->authorise('core.edit.state', 'com_issues');
					?>
					<tr class="row<?php echo $i % 2; ?>">

						<?php if (isset($this->items[0]->ordering)) : ?>
							<td class="order nowrap center hidden-phone">
								<?php if ($canChange) :
									$disableClassName = '';
									$disabledLabel    = '';

									if (!$saveOrder) :
										$disabledLabel    = JText::_('JORDERINGDISABLED');
										$disableClassName = 'inactive tip-top';
									endif; ?>
									<span class="sortable-handler hasTooltip <?php echo $disableClassName ?>"
										  title="<?php echo $disabledLabel ?>">
							<i class="icon-menu"></i>
						</span>
									<input type="text" style="display:none" name="order[]" size="5"
										   value="<?php echo $item->ordering; ?>" class="width-20 text-area-order "/>
								<?php else : ?>
									<span class="sortable-handler inactive">
							<i class="icon-menu"></i>
						</span>
								<?php endif; ?>
							</td>
						<?php endif; ?>
						<td class="hidden-phone">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
						</td>
						<?php if (isset($this->items[0]->state)): ?>
							<td class="center">
								<?php echo JHtml::_('jgrid.published', $item->state, $i, 'issues.', $canChange, 'cb'); ?>
</td>
						<?php endif; ?>

										<td>

					<?php echo $item->id; ?>
				</td>				<td class="hidden">

					<?php echo $item->created_by; ?>
				</td>				<td class="hidden">

					<?php echo $item->modified_by; ?>
				</td>				<td>
				<?php if (isset($item->checked_out) && $item->checked_out && ($canEdit || $canChange)) : ?>
					<?php echo JHtml::_('jgrid.checkedout', $i, $item->uEditor, $item->checked_out_time, 'issues.', $canCheckin); ?>
				<?php endif; ?>
				<?php if ($canEdit) : ?>
					<a href="<?php echo JRoute::_('index.php?option=com_issues&task=issue.edit&id='.(int) $item->id); ?>">
					<?php echo $this->escape($item->issue_title); ?></a>
				<?php else : ?>
					<?php echo $this->escape($item->issue_title); ?>
				<?php endif; ?>

				</td>				<td>

					<?php echo $item->category; ?>
				</td>				<td class="hidden">

					<?php echo $item->description; ?>
				</td>				<td class="hidden">

					<?php echo $item->attachments; ?>
				</td>				<td class="hidden">

					<?php
						if (!empty($item->file_upload)) :
							$file_uploadArr = explode(',', $item->file_upload);
							foreach ($file_uploadArr as $fileSingle) :
								if (!is_array($fileSingle)) :
									$uploadPath = 'docs' .DIRECTORY_SEPARATOR . $fileSingle;
									echo '<a href="' . JRoute::_(JUri::root() . $uploadPath, false) . '" target="_blank" title="See the file_upload">' . $fileSingle . '</a> | ';
								endif;
							endforeach;
						else:
							echo $item->file_upload;
						endif; ?>
				</td>				<td class="hidden">

					<?php echo $item->manuals; ?>
				</td>				<td class="hidden">

					<?php echo $item->research; ?>
					</td>				<td class="hidden">

					<?php echo $item->first_key_player; ?>
				</td>				<td class="hidden">

					<?php echo $item->issue_players; ?>
				</td>				<td class="hidden">

					<?php echo $item->published; ?>
				</td>				<td class="hidden">

					<?php echo $item->created; ?>
				</td>				<td class="hidden">

					<?php echo $item->modified; ?>
				</td>

					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>

			<input type="hidden" name="task" value=""/>
			<input type="hidden" name="boxchecked" value="0"/>
            <input type="hidden" name="list[fullorder]" value="<?php echo $listOrder; ?> <?php echo $listDirn; ?>"/>
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>
<script>
    window.toggleField = function (id, task, field) {

        var f = document.adminForm, i = 0, cbx, cb = f[ id ];

        if (!cb) return false;

        while (true) {
            cbx = f[ 'cb' + i ];

            if (!cbx) break;

            cbx.checked = false;
            i++;
        }

        var inputField   = document.createElement('input');

        inputField.type  = 'hidden';
        inputField.name  = 'field';
        inputField.value = field;
        f.appendChild(inputField);

        cb.checked = true;
        f.boxchecked.value = 1;
        window.submitform(task);

        return false;
    };
</script>