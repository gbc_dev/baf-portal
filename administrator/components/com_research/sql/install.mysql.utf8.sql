CREATE TABLE IF NOT EXISTS `#__research_publications` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`contributor_advisor` VARCHAR(255)  NOT NULL ,
`contributor_author` VARCHAR(255)  NOT NULL ,
`date_available` DATE NOT NULL ,
`identifier_uri` VARCHAR(255)  NOT NULL ,
`description` TEXT NOT NULL ,
`publisher` VARCHAR(255)  NOT NULL ,
`rights` TEXT NOT NULL ,
`subject` VARCHAR(255)  NOT NULL ,
`title` VARCHAR(255)  NOT NULL ,
`type` TEXT NOT NULL ,
`file_upload` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;

