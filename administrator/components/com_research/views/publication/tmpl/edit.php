<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Research
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_research/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {

	js('input:hidden.type').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('typehidden')){
			js('#jform_type option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_type").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'publication.cancel') {
			Joomla.submitform(task, document.getElementById('publication-form'));
		}
		else {

			if (task != 'publication.cancel' && document.formvalidator.isValid(document.id('publication-form'))) {

	if(js('#jform_type option:selected').length == 0){
		js("#jform_type option[value=0]").attr('selected','selected');
	}
				Joomla.submitform(task, document.getElementById('publication-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_research&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="publication-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_RESEARCH_TITLE_PUBLICATION', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php echo $this->form->renderField('created_by'); ?>
				<?php echo $this->form->renderField('modified_by'); ?>
				<?php echo $this->form->renderField('title'); ?>
				<?php echo $this->form->renderField('subject'); ?>
				<?php echo $this->form->renderField('type'); ?>
				<?php //echo $this->form->renderField('contributor_advisor'); ?>
				<?php echo $this->form->renderField('contributor_author'); ?>
				<?php echo $this->form->renderField('date_available'); ?>
				<?php //echo $this->form->renderField('identifier_uri'); ?>
				<?php echo $this->form->renderField('external_uri'); ?>
				<?php echo $this->form->renderField('description'); ?>
				<?php echo $this->form->renderField('publisher'); ?>
				<?php //echo $this->form->renderField('rights'); ?>

			<?php
				foreach((array)$this->item->type as $value):
					if(!is_array($value)):
						echo '<input type="hidden" class="type" name="jform[typehidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('file_upload'); ?>

				<?php if (!empty($this->item->file_upload)) : ?>
					<?php $file_uploadFiles = array(); ?>
					<?php foreach ((array)$this->item->file_upload as $fileSingle) : ?>
						<?php if (!is_array($fileSingle)) : ?>
							<a href="<?php echo JRoute::_(JUri::root() . 'research' . DIRECTORY_SEPARATOR . $fileSingle, false);?>"><?php echo $fileSingle; ?></a> |
							<?php $file_uploadFiles[] = $fileSingle; ?>
						<?php endif; ?>
					<?php endforeach; ?>
					<input type="hidden" name="jform[file_upload_hidden]" id="jform_file_upload_hidden" value="<?php echo implode(',', $file_uploadFiles); ?>" />
				<?php endif; ?>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php if (JFactory::getUser()->authorise('core.admin','research')) : ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL', true)); ?>
		<?php echo $this->form->getInput('rules'); ?>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
<?php endif; ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
