<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Manuals_factsheets
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_manuals_factsheets'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Manuals_factsheets', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('Manuals_factsheetsHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'manuals_factsheets.php');

$controller = JControllerLegacy::getInstance('Manuals_factsheets');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
