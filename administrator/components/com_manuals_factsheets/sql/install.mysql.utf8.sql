CREATE TABLE IF NOT EXISTS `#__manuals_factsheets` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`document_name` VARCHAR(255)  NOT NULL ,
`note` TEXT NOT NULL ,
`file_name` VARCHAR NOT NULL ,
`file_upload` TEXT NOT NULL ,
`published` INT NOT NULL ,
`created` DATETIME NOT NULL ,
`modified` DATETIME NOT NULL ,
`publish_up` DATE NOT NULL ,
`publish_down` DATE NOT NULL ,
`access` INT(11)  NOT NULL ,
`category` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;

