/**
* @package Helix3 Framework
* @author JoomShaper http://www.joomshaper.com
* @copyright Copyright (c) 2010 - 2015 JoomShaper
* @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/
jQuery(function($) {

    $('#offcanvas-toggler').on('click', function(event){
        event.preventDefault();
        $('body').addClass('offcanvas');
    });

    $( '<div class="offcanvas-overlay"></div>' ).insertBefore( '.body-innerwrapper > .offcanvas-menu' );

    $('.close-offcanvas, .offcanvas-overlay').on('click', function(event){
        event.preventDefault();
        $('body').removeClass('offcanvas');
    });
    
    //Mega Menu
    $('.sp-megamenu-wrapper').parent().parent().css('position','static').parent().css('position', 'relative');
    $('.sp-menu-full').each(function(){
        $(this).parent().addClass('menu-justify');
    });

    //Sticky Menu
    $(document).ready(function(){
        $("body.sticky-header").find('#sp-main-menu').sticky({topSpacing:0})
    });

    // Slideshow disaper and conflict with motools
    var carousel = jQuery('.carousel');
    if(carousel){
        if (typeof jQuery != 'undefined' && typeof MooTools != 'undefined' ) {
            Element.implement({
                slide: function(how, mode){
                    return this;
                }
            });
        }
    }

    // j2store-cart
    $('.j2store-cart-module .cart-icon').on('click', function () {
        $(this).toggleClass('active');
    });

    // share articles
    $('.share-button').on('click', function () {
        $(this).toggleClass('active');
    });

    // j2store input control
    $('.product-qty .j2store-qty-up').on('click', function() {
        $('.product-qty input.qty-details').val( parseInt($('.product-qty input.qty-details').val(), 10) + 1);
    });

    $('.product-qty .j2store-qty-down').on('click', function() {
        if ($('.product-qty input.qty-details').val() > 1) {
            $('.product-qty input.qty-details').val( parseInt($('.product-qty input.qty-details').val(), 10) - 1);
        }
    });

    //tab disapear
    $(document).off('click.tab.data-api');
    $(document).on('click.tab.data-api', '[data-toggle="tab"]', function (e) {
        e.preventDefault();
        var tab = $($(this).attr('href'));
        var activate = !tab.hasClass('active');
        $('div.tab-content>div.tab-pane.active').removeClass('active');
        $('ul.nav.nav-tabs>li.active').removeClass('active');
        if (activate) {
            $(this).tab('show')
        }
    });


    // has social share
    if ( $( ".sppb-post-share-social" ).length || $( ".helix-article-social-share-icon" ).length) {
        // social share
        $('.prettySocial').prettySocial();
    };


    $(document).ready(function () {
    var shareUrl = 'http://joomshaper.com';
    $.getJSON('http://share-count.appspot.com/?url=' + encodeURIComponent(shareUrl) + "&callback=?", function (data) {
            shares = data.shares;
            $(".count").each(function (index, el) {
                service = $(el).parents(".share-btn").attr("data-service");
                count = shares[service];
                if(count>1000) {
                    count = (count / 1000).toFixed(1);
                    if(count>1000) count = (count / 1000).toFixed(1) + "M";
                    else count = count + "k";
                }
                $(el).html(count);
            });
        });
    });
    
    //Tooltip
    $('[data-toggle="tooltip"]').tooltip();

    // // fix conflicting with mootools
    if (typeof MooTools !== 'undefined') {
      var mHide = Element.prototype.hide;
      Element.implement({
         hide: function() {
            if ($('[data-toggle="tooltip"]').attr('itemprop')) {
               return this;
            }
            mHide.apply(this, arguments);
         }
      });
    }

    
    $(document).on('click', '.sp-rating .star', function(event) {
        event.preventDefault();

        var data = {
            'action':'voting',
            'user_rating' : $(this).data('number'),
            'id' : $(this).closest('.post_rating').attr('id')
        };

        var request = {
                'option' : 'com_ajax',
                'plugin' : 'helix3',
                'data'   : data,
                'format' : 'json'
            };

        $.ajax({
            type   : 'POST',
            data   : request,
            beforeSend: function(){
                $('.post_rating .ajax-loader').show();
            },
            success: function (response) {
                var data = $.parseJSON(response.data);

                $('.post_rating .ajax-loader').hide();

                if (data.status == 'invalid') {
                    $('.post_rating .voting-result').text('You have already rated this entry!').fadeIn('fast');
                }else if(data.status == 'false'){
                    $('.post_rating .voting-result').text('Somethings wrong here, try again!').fadeIn('fast');
                }else if(data.status == 'true'){
                    var rate = data.action;
                    $('.voting-symbol').find('.star').each(function(i) {
                        if (i < rate) {
                           $( ".star" ).eq( -(i+1) ).addClass('active');
                        }
                    });

                    $('.post_rating .voting-result').text('Thank You!').fadeIn('fast');
                }

            },
            error: function(){
                $('.post_rating .ajax-loader').hide();
                $('.post_rating .voting-result').text('Failed to rate, try again!').fadeIn('fast');
            }
        });
    });

});