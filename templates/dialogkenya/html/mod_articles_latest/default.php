<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<div class="latestnews<?php echo $moduleclass_sfx; ?>">
<?php foreach ($list as $item) {

	$attrbs 		= json_decode($item->attribs);
	$images 		= json_decode($item->images);
	$intro_image 	= '';
	
	$item->catUrl =JRoute::_(ContentHelperRoute::getCategoryRoute($item->catid));

	if(isset($attrbs->spfeatured_image) && $attrbs->spfeatured_image != '') {

		$intro_image = $attrbs->spfeatured_image;
		$basename = basename($intro_image);
		$list_image = JPATH_ROOT . '/' . dirname($intro_image) . '/' . JFile::stripExt($basename) . '_small.' . JFile::getExt($basename);
		if(file_exists($list_image)) {
			$thumb_image = JURI::root(true) . '/' . dirname($intro_image) . '/' . JFile::stripExt($basename) . '_small.' . JFile::getExt($basename);
		}

	} elseif(isset($images->image_intro) && !empty($images->image_intro)) {
		$thumb_image = $images->image_intro;
	}

?>
	<div itemscope itemtype="http://schema.org/Article">
		<?php if (!empty($thumb_image)) {?>
			<div class="img-responsive article-list-img">
				<a href="<?php echo $item->link; ?>" itemprop="url">
					<img src="<?php echo $thumb_image; ?>">
				</a>
			</div>
		<?php } ?>

		<?php if($item->params->get('show_category')){ ?>
			<a href="<?php echo $item->catUrl; ?>" class="category-title"> <?php echo $item->category_title; ?> </a>
		<?php } ?>

		<a href="<?php echo $item->link; ?>" class="news365-news-title" itemprop="url">
			<span itemprop="name">
				<?php echo $item->title; ?>
			</span>
		</a>

		<?php if($item->params->get('show_publish_date')){ ?>
			
			<p class="published-date"> <?php echo JHtml::_('date', $item->publish_up, 'DATE_FORMAT_LC'); ?> </p>
		<?php } ?>
		
	</div>
<?php } ?>
</div>
