<?php
/**
* @package Helix3 Framework
* @author JoomShaper http://www.joomshaper.com
* @copyright Copyright (c) 2010 - 2016 JoomShaper
* @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or Later
*/  

//no direct accees
defined ('_JEXEC') or die ('resticted aceess');

$k2ItemModel = JPATH_SITE . '/components/com_k2/models/itemlist.php';

if(!file_exists($k2ItemModel)) {
	$k2cats = array(''=>'K2 Isn\'t installed');
} else {
	require_once $k2ItemModel;
	$k2ListModel = new K2ModelItemlist();
	$k2CatTrees  = $k2ListModel->getCategoriesTree();

	//$k2cats = array();
	$k2cats = array(''=> JText::_('COM_SPPAGEBUILDER_ADDON_ARTICLE_ALL_CAT') 	);
	foreach( $k2CatTrees as $key=> $category ){
		if ($category->parent == 0) {
			$k2cats[$category->id] 	= $category->name;
			if(isset($category->children) && $category->children){
				foreach ($category->children as $key => $cat_child) {
					$k2cats[$cat_child->id]	= '-' . $cat_child->name;
				}
			}
		}
	}

}

