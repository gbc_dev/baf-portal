<?php
/**
 * @package SP Page Builder
 * @author JoomShaper http://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2016 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/
//no direct accees
defined ('_JEXEC') or die ('resticted aceess');

class SppagebuilderAddonArticles extends SppagebuilderAddons{
    public function render() {
      $settings = $this->addon->settings;

      $title            = (isset($settings->title)) ? $settings->title : '';
      $heading_selector = (isset($settings->heading_selector) && $settings->heading_selector ) ? $settings->heading_selector : 'h3';
      $layout           = ( isset($settings->layout) && $settings->layout ) ? $settings->layout : 'default';
      $leading_item     = (isset($settings->leading_item) && $settings->leading_item) ? $settings->leading_item : '';
      $resource         = (isset($settings->resource) && $settings->resource) ? $settings->resource : 'article';
      $catid            = (isset($settings->catid) && $settings->catid) ? $settings->catid : '';
      $k2catid          = (isset($settings->k2catid) && $settings->k2catid) ? $settings->k2catid : '';
      $see_all          = (isset($settings->see_all) && $settings->see_all) ? $settings->see_all : 0;
      $social_share     = (isset($settings->social_share) && $settings->social_share) ? $settings->social_share : 0;
      $ordering         = (isset($settings->ordering) && $settings->ordering) ? $settings->ordering : '';
      $limit            = (isset($settings->limit) && $settings->limit) ? $settings->limit : '';
      $columns          = (isset($settings->columns) && $settings->columns) ? $settings->columns : '';
      $hide_thumbnail   = (isset($settings->hide_thumbnail) && $settings->hide_thumbnail) ? $settings->hide_thumbnail : '';

      $show_intro           = (isset($settings->show_intro) && $settings->show_intro) ? $settings->show_intro : '';
      $show_intro_item      = (isset($settings->show_intro_item) && $settings->show_intro_item) ? $settings->show_intro_item : '';
      $leading_intro_limit  = (isset($settings->leading_intro_limit) && $settings->leading_intro_limit) ? $settings->leading_intro_limit : '';
      $intro_intro_limit    = (isset($settings->intro_intro_limit) && $settings->intro_intro_limit) ? $settings->intro_intro_limit : '';
      $show_author          = (isset($settings->show_author) && $settings->show_author) ? $settings->show_author : '';
      $show_category        = (isset($settings->show_category) && $settings->show_category) ? $settings->show_category : '';
      $show_date            = (isset($settings->show_date) && $settings->show_date) ? $settings->show_date : '';
      $show_readmore        = (isset($settings->show_readmore) && $settings->show_readmore) ? $settings->show_readmore : '';
      $link_articles        = (isset($settings->link_articles) && $settings->link_articles) ? $settings->link_articles : '';

      $all_articles_btn_text  = (isset($settings->all_articles_btn_text) && $settings->all_articles_btn_text) ? $settings->all_articles_btn_text : '';
      $all_articles_btn_size  = (isset($settings->all_articles_btn_size) && $settings->all_articles_btn_size) ? $settings->all_articles_btn_size : '';
      $all_articles_btn_type  = (isset($settings->all_articles_btn_type) && $settings->all_articles_btn_type) ? $settings->all_articles_btn_type : '';
      $all_articles_btn_icon  = (isset($settings->all_articles_btn_icon) && $settings->all_articles_btn_icon) ? $settings->all_articles_btn_icon : '';
      $all_articles_btn_block = (isset($settings->all_articles_btn_block) && $settings->all_articles_btn_block) ? $settings->all_articles_btn_block : '';

      $class = (isset($settings->class)) ? ' '.$settings->class : '';

      if ($resource == 'k2') {
    		require_once dirname(dirname( __DIR__ )) . '/helpers/k2.php';
    		$items = SppagebuilderHelperK2::getItems($limit, $ordering, $k2catid, true);
    	} else {
    		require_once JPATH_COMPONENT . '/helpers/articles.php';
    		$items = SppagebuilderHelperArticles::getArticles($limit, $ordering, $catid);
    	}

    	if(count($items)) {
    		$output  = '<div class="sppb-addon sppb-addon-articles' . $class . ' layout-' . $layout. '">';
    		if($title) {
            $output .= '<'.$heading_selector.' class="sppb-addon-title">' . $title . '</'.$heading_selector.'>';
    		}

    		$cat_url = '';
    		if ($see_all && $catid !='all' && $catid) {
    			if ($resource == 'k2') {
    				$cat_url = urldecode(JRoute::_(K2HelperRoute::getCategoryRoute($catid.':'.urlencode($catid))));
    			} else{
    				$cat_url = JRoute::_(ContentHelperRoute::getCategoryRoute($catid, $catid));
    			}

    			$output .= '<div class="sppb-addon-section-right">';
    			$output .= '<a href="' . $cat_url . '">' . JText::_('COM_SPPAGEBUILDER_ADDON_LP_SEE_ALL') . '</a>';
    			$output .= '</div>';
    		}

    		$output .= '<div class="sppb-addon-content">';
    		$output .= '<div class="sppb-row">';

    		$i= 0;

    		$total_item = count($items);

    		foreach ($items as $key => $item) {
    			$key ++;

    			if ($resource == 'k2') {
    				$item->catUrl = urldecode(JRoute::_(K2HelperRoute::getCategoryRoute($item->catid.':'.urlencode($item->category_alias))));
    			} else {
    				$item->catUrl = JRoute::_(ContentHelperRoute::getCategoryRoute($item->catslug));
    			}

    			$image = '';
    			$item_type = '';
    			if ( $key  <= $leading_item && $layout == 'featured' ) {
    				$image = (isset($item->image_medium) && $item->image_medium) ? $item->image_medium : '';
    				$column = 12;
    				$item_type = 'leading-item';
    			} elseif( $key  <= $leading_item && $layout == 'business' ) {
    				$image = (isset($item->image_medium) && $item->image_medium) ? $item->image_medium : '';
    				$column = 6;
    				$item_type = 'leading-item';
    			} elseif( ( ( $key  > $leading_item && $key == (is_numeric($leading_item) +1) ) ) && $layout == 'business' ) {
    				$image = (isset($item->image_small) && $item->image_small) ? $item->image_small : '';
    				$column = 6;
    				$item_type = 'intro-item';
    			} else {
    				$image = (isset($item->image_small) && $item->image_small) ? $item->image_small : '';
    				$column = round(12/$columns);
    				$item_type = 'intro-item';
    			}

    			if( ( $key == 1 && $key  <= $leading_item) || ( $key  > $leading_item && $key == (is_numeric($leading_item) +1)  ) && ($layout == 'business') ) {
    				//$output .= '<div class="sppb-col-sm-'. $column .'">';
    				$output .= '<div class="sppb-col-sm-'. $column .'">';
    			} elseif($layout != 'business'){
    				$output .= '<div class="sppb-col-sm-'. $column .'">';
    			}

    			if( ( $key  > $leading_item && $key == (is_numeric($leading_item) +1)  ) && ($layout == 'business') ) {
    				$output .= '<div class="sppb-row">';
    			}

    			if ( $key  > $leading_item && $layout == 'business' ) {
    				$output .= '<div class="sppb-col-sm-'. $column .'">';
    			}

    			$output .= '<div class="sppb-addon-article ' . $item_type . ' ">';

    			if(!$hide_thumbnail) {
    				if($resource != 'k2' && $item->post_format=='gallery') {
    					if(count($item->imagegallery->images)) {
    						$output .= '<div class="sppb-carousel sppb-slide" data-sppb-ride="sppb-carousel">';
    						$output .= '<div class="sppb-carousel-inner">';
    						foreach ($item->imagegallery->images as $gallery_item) {
    							$output .= '<div class="sppb-item">';
    							$output .= '<img src="'. $gallery_item['thumbnail'] .'" alt="'. $item->title .'">';
    							$output .= '</div>';
    						}
    						$output	.= '</div>';

    						$output	.= '<a class="left sppb-carousel-control" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>';
    						$output	.= '<a class="right sppb-carousel-control" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>';

    						$output .= '</div>';

    					} elseif (isset($item->image_thumbnail) && $item->image_thumbnail) {
    						$output .= '<a href="'. $item->link .'" class="sppb-img-wrapper" itemprop="url"><img class="sppb-img-responsive" src="'. $image .'" alt="'. $item->title .'" itemprop="thumbnailUrl"></a>';
    					}
    				} else {
    					if(isset($item->image_thumbnail) && $item->image_thumbnail && $image) {
    						$output .= '<a href="'. $item->link .'" class="sppb-img-wrapper" itemprop="url"><img class="sppb-img-responsive" src="'. $image .'" alt="'. $item->title .'" itemprop="thumbnailUrl"></a>';
    					}
    				}
    			}

    			$output .= '<div class="sppb-article-details">';
    				if($show_category) {
    					$output .= '<span class="sppb-meta-category"><a href="'. $item->catUrl .'" itemprop="genre">' . $item->category . '</a></span>';
    				}

    				$output .= '<h3 class="sppb-article-title"><a href="'. $item->link .'" itemprop="url">' . $item->title . '</a></h3>';

    				if($key  <= $leading_item && $show_intro) {
    					$output .= '<div class="sppb-article-introtext">'. Jhtml::_('string.truncate', ($item->introtext), $leading_intro_limit) .'</div>';
    				} elseif($key  > $leading_item && $key == (is_numeric($leading_item) +1) && $show_intro_item){
    					$output .= '<div class="sppb-article-introtext">'. Jhtml::_('string.truncate', ($item->introtext), $intro_intro_limit) .'</div>';
    				}

    				if($show_readmore) {
    					$output .= '<a class="sppb-readmore" href="'. $item->link .'" itemprop="url">'. $readmore_text .'</a>';
    				}

    				if($show_author || $show_date) {
    					$output .= '<div class="sppb-article-meta">';
    						if($show_date) {
    							$output .= '<span class="sppb-meta-date" itemprop="dateCreated">' . Jhtml::_('date', $item->created, 'DATE_FORMAT_LC3') . '</span>';
    						}

    						if($show_author) {
    							$output .= '<span class="sppb-meta-author" itemprop="name">' . $item->username . '</span>';
    						}
                // social share
    						if($social_share) {
    							$output .= '<div class="sppb-post-share-social">';
    							$output .= '<a href="#" data-type="facebook" data-url="'. $_SERVER['SERVER_NAME'] . $item->link .'" data-title="'. $item->title .'" data-media="' . $image .'" class="prettySocial fa fa-facebook"></a>';
    							$output .= '<a href="#" data-type="twitter" data-url="'. $_SERVER['SERVER_NAME'] . $item->link .'" data-description="'. $item->title .'" data-media="' . $image .'" data-via="joomshaper" class="prettySocial fa fa-twitter"></a>';
    							$output .= '<span class="share-button"><i class="fa fa-share-alt"></i></span>'; //.social share others
    							$output .= '<div class="sppb-post-share-social-others">';
    							$output .= '<a href="#" data-type="googleplus" data-url="'. $item->link .'" data-description="'. $item->title .'" data-media="' . $image .'" class="prettySocial fa fa-google-plus"></a>';
    							$output .= '<a href="#" data-type="pinterest" data-url="'. $item->link .'" data-description="'. $item->title .'" data-media="' . $image .'" class="prettySocial fa fa-pinterest"></a>';
    							$output .= '<a href="#" data-type="linkedin" data-url="'. $item->link .'" data-title="'. $item->title .'" data-description="'. $item->title .'" data-via="joomshaper" data-media="" class="prettySocial fa fa-linkedin"></a>';
    							$output .= '</div>'; //.social share others
    							$output .= '</div>'; //.social share
    						}
    					$output .= '</div>'; //.sppb-article-meta
    				}
    				$output .= '</div>';
    			$output .= '</div>'; //.sppb-article-details

    			if ( $key  > $leading_item && $layout == 'business' ) {
    				$output .= '</div>'; // sppb-col inner
    			}

    			if( $key == $total_item && $layout == 'business'){
    				$output .= '</div>'; // sppb-row
    			}

    			if( ($key == $leading_item) || ( $key == $total_item && $layout == 'business') ) {
    				$output .= '</div>'; // col-sm
    			} elseif($layout != 'business'){
    				$output .= '</div>'; // col-sm
    			}

    			$i ++;
    		}
    		$output .= '</div>';

    		// See all link
    		if($link_articles) {
    			if($all_articles_btn_icon !='') {
    				$all_articles_btn_text = '<i class="fa ' . $all_articles_btn_icon . '"></i> ' . $all_articles_btn_text;
    			}

    			if ($resource == 'k2') {
    				$output  .= '<a href="' . urldecode(JRoute::_(K2HelperRoute::getCategoryRoute($catid.':'.urlencode($catid)))) . '" class="sppb-btn sppb-btn-' . $all_articles_btn_type . ' sppb-btn-' . $all_articles_btn_size . ' ' . $all_articles_btn_block . '" role="button">' . $all_articles_btn_text . '</a>';
    			} else{
    				$output  .= '<a href="' . JRoute::_(ContentHelperRoute::getCategoryRoute($catid)) . '" class="sppb-btn sppb-btn-' . $all_articles_btn_type . ' sppb-btn-' . $all_articles_btn_size . ' ' . $all_articles_btn_block . '" role="button">' . $all_articles_btn_text . '</a>';
    			}
    		}

    		$output .= '</div>';
    		$output .= '</div>';

    		return $output;
    	}

    	return false;
    }

    public function scripts() {
      $app = JFactory::getApplication();
      $base_path = JURI::base() . '/templates/' . $app->getTemplate() . '/js/';
      return array($base_path . 'jquery.prettySocial.min.js');
  	}

    public function css() {
      $addon_id = '#sppb-addon-' . $this->addon->id;
      $settings = $this->addon->settings;
      $title = (isset($settings->title)) ? $settings->title : '';
  		$css = '';

      $title_style = (isset($settings->title_margin_top) && $settings->title_margin_top ) ? 'margin-top:' . (int) $settings->title_margin_top . 'px;' : '';
      $title_style .= (isset($settings->title_margin_bottom) && $settings->title_margin_bottom ) ? 'margin-bottom:' . (int) $settings->title_margin_bottom . 'px;' : '';
      $title_style .= (isset($settings->title_text_color) && $settings->title_text_color) ? 'color:' . $settings->title_text_color  . ';' : '';
      $title_style .= (isset($settings->title_fontsize) && $settings->title_fontsize) ? 'font-size:'. $settings->title_fontsize .'px;line-height:'. $settings->title_fontsize .'px;' : '';
      $title_style .= (isset($settings->title_fontweight) && $settings->title_fontweight) ? 'font-weight:'. $settings->title_fontweight .';' : '';

      if($title_style && $title) {
				$css .= $addon_id . ' .sppb-addon-title {';
				$css .= $title_style;
				$css .= '}';
			}

      return $css;
    }
}
