<?php
/**
 * @package SP Page Builder
 * @author JoomShaper http://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2015 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/
//no direct accees
defined ('_JEXEC') or die ('resticted aceess');

class SppagebuilderAddonImage_content extends SppagebuilderAddons {
		public function render() {
			$settings = $this->addon->settings;

			$image 							= (isset($settings->image) && $settings->image) ? $settings->image : '';
			$image_width 				= (isset($settings->image_width) && $settings->image_width) ? $settings->image_width : '';
			$image_alignment 		= (isset($settings->image_alignment) && $settings->image_alignment) ? $settings->image_alignment : '';
			$title 							= (isset($settings->title) && $settings->title) ? $settings->title : '';
			$heading_selector 	= (isset($settings->heading_selector) && $settings->heading_selector) ? $settings->heading_selector : '';
			$text 							= (isset($settings->text) && $settings->text) ? $settings->text : '';
			$button_text 				= (isset($settings->button_text) && $settings->button_text) ? $settings->button_text : '';
			$button_url 				= (isset($settings->button_url) && $settings->button_url) ? $settings->button_url : '';
			$button_size 				= (isset($settings->button_size) && $settings->button_size) ? $settings->button_size : '';
			$button_type 				= (isset($settings->button_type) && $settings->button_type) ? $settings->button_type : '';
			$button_icon 				= (isset($settings->button_icon) && $settings->button_icon) ? $settings->button_icon : '';
			$button_block 			= (isset($settings->button_block) && $settings->button_block) ? $settings->button_block : '';
			$button_target 			= (isset($settings->button_target) && $settings->button_target) ? $settings->button_target : '';
			$class 							= (isset($settings->class) && $settings->class) ? ' '.$settings->class : '';

			if($image_alignment=='left') {
				$eontent_class = ' sppb-col-sm-offset-6';
			} else {
				$eontent_class = '';
			}

			if($image && $title) {

				$output  = '<div class="sppb-addon sppb-addon-image-content aligment-'. $image_alignment .' clearfix ' . $class . '">';
				$output .= '<div style="background-image: url(' .  $image . ');" class="sppb-image-holder">';
				$output .= '</div>';
				$output .= '<div class="sppb-container">';
				$output .= '<div class="sppb-row">';
				$output .= '<div class="sppb-col-sm-6'. $eontent_class .'">';
				$output .= '<div class="sppb-content-holder">';

				$output .= '<'.$heading_selector.' class="sppb-image-content-title">' . $title . '</'.$heading_selector.'>';
				if($text) $output .= '<p class="sppb-image-content-text">' . $text . '</p>';
				if($button_icon) {
					$button_text = '<i class="fa ' . $button_icon . '"></i> ' . $button_text;
				}

				if($button_text) {
					$output .= '<a target="' . $button_target . '" href="' . $button_url . '" class="sppb-btn sppb-btn-' . $button_type . ' sppb-btn-' . $button_size . ' ' . $button_block . '" role="button">' . $button_text . '</a>';
				}

				$output .= '</div>';
				$output .= '</div>';
				$output .= '</div>';
				$output .= '</div>';
				$output .= '</div>';

				return $output;
			}
			return;
		}

		public function css() {
  		$addon_id = '#sppb-addon-' . $this->addon->id;
      $settings = $this->addon->settings;
  		$css = '';
      $title = (isset($settings->title)) ? $settings->title : '';

      $title_style     = (isset($settings->title_margin_top) && $settings->title_margin_top ) ? 'margin-top:' . (int) $settings->title_margin_top . 'px;' : '';
      $title_style     .= (isset($settings->title_margin_bottom) && $settings->title_margin_bottom ) ? 'margin-bottom:' . (int) $settings->title_margin_bottom . 'px;' : '';
      $title_style     .= (isset($settings->title_fontsize) && $settings->title_fontsize ) ? 'font-size:'.$settings->title_fontsize.'px;line-height:'.$settings->title_fontsize.'px;' : '';
      $title_style     .= (isset($settings->title_fontweight) && $settings->title_fontweight ) ? 'font-weight:'.$settings->title_fontweight.';' : '';
      $title_style     .= (isset($settings->title_text_color) && $settings->title_text_color ) ? 'color:' . $settings->title_text_color  . ';' : '';

      if($title_style && $title) {
				$css .= $addon_id . ' .sppb-image-content-title {';
				$css .= $title_style;
				$css .= '}';
			}
      return $css;
  	}
}
