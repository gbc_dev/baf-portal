<?php
/**
 * @package SP Page Builder
 * @author JoomShaper http://www.joomshaper.com
 * @copyright Copyright (c) 2010 - 2016 JoomShaper
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2 or later
*/
//no direct accees
defined ('_JEXEC') or die ('restricted aceess');

class SppagebuilderAddonAjax_contact extends SppagebuilderAddons{

	public function render() {

		$settings = $this->addon->settings;

		$class 						= ( isset($settings->class) && $settings->class) ? ' '.$settings->class : '';
		$title 						= ( isset($settings->title) && $settings->title) ? $settings->title : '';
		$heading_selector = ( isset($settings->heading_selector) && $settings->heading_selector) ? $settings->heading_selector : '';
		$recipient_email 	= ( isset($settings->recipient_email) && $settings->recipient_email) ? $settings->recipient_email : '';
		$formcaptcha 			= ( isset($settings->formcaptcha) && $settings->formcaptcha) ? $settings->formcaptcha : '';
		$captcha_question = ( isset($settings->captcha_question) && $settings->captcha_question) ? $settings->captcha_question : '';
		$captcha_answer 	= ( isset($settings->captcha_answer) && $settings->captcha_answer) ? $settings->captcha_answer : '';

		$output  = '<div class="sppb-addon sppb-addon-ajax-contact' . $class . '">';
		if($title) {
			$output .= '<'.$heading_selector.' class="sppb-addon-title">' . $title . '</'.$heading_selector.'>';
		}

		$output .= '<div class="sppb-addon-content">';
		$output .= '<form class="sppb-ajaxt-contact-form">';
		$output .= '<div class="sppb-form-group">';
		$output .= '<input type="text" name="name" class="sppb-form-control" placeholder="'. JText::_('COM_SPPAGEBUILDER_ADDON_AJAX_CONTACT_NAME') .'" required="required">';
		$output .= '</div>';
		$output .= '<div class="sppb-form-group">';
		$output .= '<input type="email" name="email" class="sppb-form-control" placeholder="'. JText::_('COM_SPPAGEBUILDER_ADDON_AJAX_CONTACT_EMAIL') .'" required="required">';
		$output .= '</div>';
		$output .= '<div class="sppb-form-group">';
		$output .= '<input type="text" name="subject" class="sppb-form-control" placeholder="'. JText::_('COM_SPPAGEBUILDER_ADDON_AJAX_CONTACT_SUBJECT') .'" required="required">';
		$output .= '</div>';

		if($formcaptcha) {
			$output .= '<div class="sppb-form-group">';
			$output .= '<input type="text" name="captcha_question" class="sppb-form-control" placeholder="'. $captcha_question .'" required="required">';
			$output .= '</div>';
		}

		$output .= '<div class="sppb-form-group">';
		$output .= '<textarea type="text" name="message" rows="5" class="sppb-form-control" placeholder="'. JText::_('COM_SPPAGEBUILDER_ADDON_AJAX_CONTACT_MESSAGE') .'" required="required"></textarea>';
		$output .= '</div>';
		$output .= '<input type="hidden" name="recipient" value="'. base64_encode($recipient_email) .'">';

		if($formcaptcha) {
			$output .= '<input type="hidden" name="captcha_answer" value="'. md5($captcha_answer) .'">';
		}
		$output .= '<button type="submit" class="sppb-btn sppb-btn-primary"><i class="fa"></i> '. JText::_('COM_SPPAGEBUILDER_ADDON_AJAX_CONTACT_SEND') .'</button>';
		$output .= '</form>';
		$output .= '<div style="display:none;margin-top:10px;" class="sppb-ajax-contact-status"></div>';
		$output .= '</div>';
		$output .= '</div>';

		return $output;
	}

	public static function getAjax() {
		$input  			= JFactory::getApplication()->input;
		$mail 				= JFactory::getMailer();
		$showcaptcha 	= false;
		$inputs 			= $input->get('data', array(), 'ARRAY');

		foreach ($inputs as $input) {

			if( $input['name'] == 'recipient' ) {
				$recipient 			= base64_decode($input['value']);
			}

			if( $input['name'] == 'email' ) {
				$email 			= $input['value'];
			}

			if( $input['name'] == 'name' ) {
				$name 			= $input['value'];
			}

			if( $input['name'] == 'subject' ) {
				$subject 			= $input['value'];
			}

			if( $input['name'] == 'message' ) {
				$message 			= nl2br( $input['value'] );
			}

			if( $input['name'] == 'captcha_question' ) {
				$captcha_question 	= $input['value'];
				$showcaptcha		= true;
			}

			if( $input['name'] == 'captcha_answer' ) {
				$captcha_answer 	= $input['value'];
				$showcaptcha		= true;
			}
		}

		if($showcaptcha) {
			if ( md5($captcha_question) != $captcha_answer ) {
				return '<span class="sppb-text-danger">'. JText::_('COM_SPPAGEBUILDER_ADDON_AJAX_CONTACT_WRONG_CAPTCHA') .'</span>';
			}
		}

		$sender = array($email, $name);
		$mail->setSender($sender);
		$mail->addRecipient($recipient);
		$mail->setSubject($subject);
		$mail->isHTML(true);
		$mail->Encoding = 'base64';
		$mail->setBody($message);

		if ($mail->Send()) {
			return '<span class="sppb-text-success">'. JText::_('COM_SPPAGEBUILDER_ADDON_AJAX_CONTACT_SUCCESS') .'</span>';
		} else {
			return '<span class="sppb-text-danger">'. JText::_('COM_SPPAGEBUILDER_ADDON_AJAX_CONTACT_FAILED') .'</span>';
		}
	}

	public function css() {
		$addon_id = '#sppb-addon-' . $this->addon->id;
		$settings = $this->addon->settings;
		$css = '';
		$title = (isset($settings->title)) ? $settings->title : '';

		$title_style     = (isset($settings->title_margin_top) && $settings->title_margin_top ) ? 'margin-top:' . (int) $settings->title_margin_top . 'px;' : '';
		$title_style     .= (isset($settings->title_margin_bottom) && $settings->title_margin_bottom ) ? 'margin-bottom:' . (int) $settings->title_margin_bottom . 'px;' : '';
		$title_style     .= (isset($settings->title_fontsize) && $settings->title_fontsize ) ? 'font-size:'.$settings->title_fontsize.'px;line-height:'.$settings->title_fontsize.'px;' : '';
		$title_style     .= (isset($settings->title_fontweight) && $settings->title_fontweight ) ? 'font-weight:'.$settings->title_fontweight.';' : '';
		$title_style     .= (isset($settings->title_text_color) && $settings->title_text_color ) ? 'color:' . $settings->title_text_color  . ';' : '';

		if($title_style && $title) {
			$css .= $addon_id . ' .sppb-addon-title {';
			$css .= $title_style;
			$css .= '}';
		}
		return $css;
	}
}
