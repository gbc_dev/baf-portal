<?php
/**
 * Helper class for Hello World! module
 * 
 * @package    Joomla.Tutorials
 * @subpackage Modules
 * @link http://docs.joomla.org/J3.x:Creating_a_simple_module/Developing_a_Basic_Module
 * @license        GNU/GPL, see LICENSE.php
 * mod_helloworld is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
class ModIssuesSearchHelper
{
    /**
     * Retrieves the hello message
     *
     * @param   array  $params An object containing the module parameters
     *
     * @access public
     */    
    public static function getHello($params)
    {
        // Get a db connection.
        // Create a new query object.       
        /*$db = &JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*');
        // $query->select('cats.description AS description, cats.title AS titel');o
        $query->from('#__research_publications');
        // $query->from('#__categories AS cats');o
        // $query->where('cats.id="1"');0
        // $query->where('cats.id="1"');
        $db->setQuery($query);
        // $Category = $db->loadObjectList();0
        $Category = $db->loadObjectList();

        var_dump($Category);*/
        // return 'Hello, World!';

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
         
        // Build the query
        $query->select($db->quoteName(array('title', 'identifier_uri')));
        $query->from($db->quoteName('#__research_publications'));
        // $query->where($db->quoteName('introtext') . ' LIKE '. $db->quote('%Joomla%'));
        // $query->order('ordering ASC');
        $query->setLimit('5');
         
        $db->setQuery($query);
        $results = $db->loadObjectList();

        // Print the result

        // foreach($results as $result){
        //     echo '<a href="#"><i class="fa fa-book fa-2x"></i> ' . $result->title . '</a>';
        //     // echo $result->introtext;
        // }
    }
}