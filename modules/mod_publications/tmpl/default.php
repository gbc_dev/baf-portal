<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_users_latest
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// var_dump($pubs);
?>
<h4><i class="fa fa-book fa-1x"></i> Research & Publications</h4>
<?php if (!empty($pubs)) : ?>
	<ul class="ltst_publications<?php echo $moduleclass_sfx; ?>" >
	<?php foreach ($pubs as $pub) : ?>
		<li>
			<a href="index.php?option=com_research&view=publication&id=<?php echo $pub->id; ?>&Itemid=703"><i class="fa fa-book fa"></i> <?php echo $pub->subject; ?> </a>
		</li>
	<?php endforeach; ?>
	</ul>
<?php endif; ?>

<br/>
<div class="row">
  <div class="col-sm-12">
    <div class="text-center">
      <a href="index.php?option=com_research&view=publications&Itemid=706" class="btn btn-primary" id="singlebutton"> Read More</a>
    </div>
  </div>
</div>