<?php
/*------------------------------------------------------------------------
# mod_2bros_twitter_displayer - 2Bros Twitter Displayer
# ------------------------------------------------------------------------
# @author - 2brothers.co.nz
# @copyright - 2brothers.co.nz
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: hhttp://2brothers.co.nz/
# Technical Support:  admin@2brothers.co.nz
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die;
require JModuleHelper::getLayoutPath('mod_2bros_twitter_displayer', $params->get('layout', 'default'));