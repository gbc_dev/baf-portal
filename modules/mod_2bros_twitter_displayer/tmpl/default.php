<?php
/*------------------------------------------------------------------------
# mod_2bros_twitter_displayer - 2Bros Twitter Displayer
# ------------------------------------------------------------------------
# @author - 2brothers.co.nz
# @copyright - 2brothers.co.nz
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: hhttp://2brothers.co.nz/
# Technical Support:  admin@2brothers.co.nz
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die;
//all parameters
$userName = $params->get('userName');
$widgetId = $params->get('widgetId');
$width = $params->get('width');
$height = $params->get('height');
$widgetTheme = $params->get('widgetTheme');
$linkColor = $params->get('linkColor');
$borderColor = $params->get('borderColor');
$count = $params->get('count');
$border = $params->get('border');
$scrollbar = $params->get('scrollbar');
$footer = $params->get('footer');
$print_twitter = '';
$print_twitter .= '<a class="twitter-timeline" data-theme="'.$widgetTheme.'" data-link-color="'.$linkColor.'" data-border-color="'.$borderColor.'"  data-chrome="'.$footer.$border.$scrollbar.'"  '.$count.'  href="https://twitter.com/'.$userName.'" width="'.$width.'" height="'.$height.'">Tweets by @'.$userName.'</a>';
$print_twitter .= '<div style="font-size: 9px; color: #808080; font-weight: normal; font-family: tahoma,verdana,arial,sans-serif; line-height: 1.28; text-align: right; direction: ltr;"></div>';
?>
<div id="sw_twitter_display" class="<?php echo $params->get('moduleclass_sfx');?>">
	<?php echo $print_twitter; ?>
</div>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
