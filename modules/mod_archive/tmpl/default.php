<?php
/**
 * @subpackage	mod_sp_date
 * @copyright	Copyright (C) 2010 - 2016 JoomShaper. All rights reserved.
 * @license		GNU General Public License version 2 or later; 
 */

// no direct access
defined('_JEXEC') or die;
?>

<div id="sp-team<?php echo $module->id; ?>" class="sp-date <?php echo $params->get('moduleclass_sfx') ?>">
    <div class="row-fluid">
    	<div class="sp-date-wrapper hidden">
    		<span>
	    	<?php
    			echo JHtml::_('date', JFactory::getDate(), JText::_('DATE_FORMAT_LC1'));
	     	?>
	     	</span>
	     </div>
	<div class="col-md-4 center-block"><img src="images/Archive.png" alt="" width="89" height="91" /></div>
	<div class="col-md-8 pull-top"><a id="btn-978323406761" class="sppb-btn sppb-btn-primary sppb-btn-lg sppb-btn-rounded" href="#" target="_self"><span style="font-size: 12px;">Visit Archive</span></a></div>
    </div><!--/.row-fluid-->
</div>