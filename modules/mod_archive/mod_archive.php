<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_sp_team
 * @copyright	Copyright (C) 2010 - 2016 JoomShaper. All rights reserved.
 * @license		GNU General Public License version 2 or later; 
 */

// no direct access
defined('_JEXEC') or die;


require JModuleHelper::getLayoutPath('mod_archive', $params->get('layout'));