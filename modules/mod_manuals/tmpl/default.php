<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_users_latest
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// var_dump($manuals);
?>
<h4><i class="fa fa-file fa-1x"></i> Manuals & Factsheets</h4>
<?php if (!empty($manuals)) : ?>
	<ul class="ltst_publications<?php echo $moduleclass_sfx; ?>" >
	<?php foreach ($manuals as $manual) : ?>
		<li>
			<a href="index.php?option=com_manuals_factsheets&view=manual&id&id=<?php echo $manual->id; ?>&Itemid=703"><i class="fa fa-file fa-1x"></i> <?php echo $manual->document_name; ?> </a>
		</li>
	<?php endforeach; ?>
	</ul>
<?php endif; ?>

<br/>
<div class="row">
  <div class="col-sm-12">
    <div class="text-center">
      <a href="index.php?option=com_manuals_factsheets&view=manuals&Itemid=545" class="btn btn-primary" id="singlebutton"> Read More</a>
    </div>
  </div>
</div>