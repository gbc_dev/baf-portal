<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_users_latest
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the latest functions only once
JLoader::register('ModLatestIssuesHelper', __DIR__ . '/helper.php');

$shownumber      = $params->get('shownumber', 5);
$names           = ModLatestIssuesHelper::getUsers($params);
$pubs           = ModLatestIssuesHelper::getPublications($params);
$issues           = ModLatestIssuesHelper::getIssues($params);
// var_dump($issues);
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');

require JModuleHelper::getLayoutPath('mod_issues', $params->get('layout', 'default'));
