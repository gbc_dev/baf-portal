<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_users_latest
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_users_latest
 *
 * @since  1.6
 */
class ModLatestIssuesHelper
{
	/**
	 * Get users sorted by activation date
	 *
	 * @param   \Joomla\Registry\Registry  $params  module parameters
	 *
	 * @return  array  The array of users
	 *
	 * @since   1.6
	 */
	public static function getUsers($params)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select($db->quoteName(array('a.id', 'a.name', 'a.username', 'a.registerDate')))
			->order($db->quoteName('a.registerDate') . ' DESC')
			->from('#__users AS a');

			$user = JFactory::getUser();

		if (!$user->authorise('core.admin') && $params->get('filter_groups', 0) == 1)
		{
			$groups = $user->getAuthorisedGroups();

			if (empty($groups))
			{
				return array();
			}

			$query->join('LEFT', '#__user_usergroup_map AS m ON m.user_id = a.id')
				->join('LEFT', '#__usergroups AS ug ON ug.id = m.group_id')
				->where('ug.id in (' . implode(',', $groups) . ')')
				->where('ug.id <> 1');
		}

		$db->setQuery($query, 0, $params->get('shownumber'));

		try
		{
			return (array) $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			JFactory::getApplication()->enqueueMessage(JText::_('JERROR_AN_ERROR_HAS_OCCURRED'), 'error');

			return array();
		}
	}


/**
	 * Get latest publications
	 *
	 * @param   \Joomla\Registry\Registry  $params  module parameters
	 *
	 * @return  array  The array of users
	 *
	 * @since   1.6
	 */
	public static function getPublications($params)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select($db->quoteName(array('a.id', 'a.issue_title', 'a.category')))
			->order($db->quoteName('a.id') . ' DESC')
			->from('#__issues AS a')
			->where('a.state = 1');

		// $query->leftJoin('b ON b.id = a.id')->leftJoin('c ON c.id = b.id');

			//Join over subcounties
		// $query->select('issue_categories.title AS ' . $db->quote('cat_name'))
		$query->select('issue_categories.title AS ' . $db->quote('cat_name'))
		->select('issue_categories.note AS ' . $db->quote('cat_color'))
			->join('LEFT', '#__categories AS issue_categories ON issue_categories.id = a.category');


			// var_dump($query);

		$user = JFactory::getUser();

		if (!$user->authorise('core.admin') && $params->get('filter_groups', 0) == 1)
		{
			$groups = $user->getAuthorisedGroups();

			if (empty($groups))
			{
				return array();
			}

			$query->join('LEFT', '#__user_usergroup_map AS m ON m.user_id = a.id')
				->join('LEFT', '#__usergroups AS ug ON ug.id = m.group_id')
				->where('ug.id in (' . implode(',', $groups) . ')')
				->where('ug.id <> 1');
		}

		$db->setQuery($query, 0, $params->get('shownumber'));

		try
		{
			return (array) $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			JFactory::getApplication()->enqueueMessage(JText::_('JERROR_AN_ERROR_HAS_OCCURRED'), 'error');

			return array();
		}

	}


/**
	 * Get latest issues
	 *
	 * @param   \Joomla\Registry\Registry  $params  module parameters
	 *
	 * @return  array  The array of users
	 *
	 * @since   1.6
	 */
	public static function getIssues($params)
	{
		// Get a db connection.
		$db = JFactory::getDbo();

		// Create a new query object.
		$query = $db->getQuery(true);

		// Select all articles for users who have a username which starts with 'a'.
		// Order it by the created date.
		// Note by putting 'a' as a second parameter will generate `#__content` AS `a`
		$query
		    ->select(array('a.*', 'b.id', 'b.title'))
		    ->from($db->quoteName('#__issues', 'a'))
		    ->join('INNER', $db->quoteName('#__categories', 'b') . ' ON (' . $db->quoteName('a.category') . ' = ' . $db->quoteName('b.id') . ')')
		    // ->where($db->quoteName('b.username') . ' LIKE \'a%\'')
		    ->order($db->quoteName('a.id') . ' DESC');

		// Reset the query using our newly populated query object.
		$db->setQuery($query);

		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$results = $db->loadObjectList();

		// var_dump($results);
	}

}
