<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_users_latest
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
// echo '<pre>';
// var_dump($pubs);
// echo '<pre/>';
?>
<!-- <h4><i class="fa fa-book fa-1x"></i> Research & Publications</h4> -->
<?php if (!empty($pubs)) : ?>
	<ul class="ltst_publications<?php echo $moduleclass_sfx; ?>" >
	<?php foreach ($pubs as $pub) : ?>
		<li>
			<a href="index.php?option=com_issues&view=issue&id=<?php echo $pub->id; ?>&Itemid=278"><i class="fa fa-info-circle" aria-hidden="true"></i> <?php echo $pub->issue_title; ?> </a> <span class="labels">
            <a href="index.php?option=com_issues&view=cat&id=<?php echo $pub->category;?>" class="label v-align-text-top labelstyle-7057ff linked-labelstyle-7057ff" style="background-color: <?php echo $pub->cat_color; ?>; color: #fff;" title="Label: good first issue"><?php echo $pub->cat_name; ?></a>
        </span>
		</li>
	<?php endforeach; ?>
	</ul>
<?php endif; ?>
