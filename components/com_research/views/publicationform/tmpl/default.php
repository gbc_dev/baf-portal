<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Research
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

// Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_research', JPATH_SITE);
$doc = JFactory::getDocument();
$doc->addScript(JUri::base() . '/media/com_research/js/form.js');

$user    = JFactory::getUser();
$canEdit = ResearchHelpersResearch::canUserEdit($this->item, $user);


?>

<div class="publication-edit front-end-edit">
	<?php if (!$canEdit) : ?>
		<h3>
			<?php throw new Exception(JText::_('COM_RESEARCH_ERROR_MESSAGE_NOT_AUTHORISED'), 403); ?>
		</h3>
	<?php else : ?>
		<?php if (!empty($this->item->id)): ?>
			<h1><?php echo JText::sprintf('COM_RESEARCH_EDIT_ITEM_TITLE', $this->item->id); ?></h1>
		<?php else: ?>
			<h1><?php echo JText::_('COM_RESEARCH_ADD_ITEM_TITLE'); ?></h1>
		<?php endif; ?>

		<form id="form-publication"
			  action="<?php echo JRoute::_('index.php?option=com_research&task=publication.save'); ?>"
			  method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
			
	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />

	<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />

	<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />

	<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />

	<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php echo $this->form->getInput('created_by'); ?>
				<?php echo $this->form->getInput('modified_by'); ?>


	<?php echo $this->form->renderField('title'); ?>
	<?php echo $this->form->renderField('subject'); ?>
	<?php echo $this->form->renderField('type'); ?>
	<?php echo $this->form->renderField('contributor_advisor'); ?>
	<?php echo $this->form->renderField('contributor_author'); ?>
	<?php echo $this->form->renderField('date_available'); ?>

	<?php //echo $this->form->renderField('identifier_uri'); ?>

	<?php echo $this->form->renderField('description'); ?>

	<?php echo $this->form->renderField('publisher'); ?>
	<?php echo $this->form->renderField('external_uri'); ?>

	<?php //echo $this->form->renderField('rights'); ?>

	<?php echo $this->form->renderField('file_upload'); ?>

				<?php if (!empty($this->item->file_upload)) : ?>
					<?php $file_uploadFiles = array(); ?>
					<?php foreach ((array)$this->item->file_upload as $fileSingle) : ?>
						<?php if (!is_array($fileSingle)) : ?>
							<a href="<?php echo JRoute::_(JUri::root() . 'research' . DIRECTORY_SEPARATOR . $fileSingle, false);?>"><?php echo $fileSingle; ?></a> | 
							<?php $file_uploadFiles[] = $fileSingle; ?>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>
				<input type="hidden" name="jform[file_upload_hidden]" id="jform_file_upload_hidden" value="<?php echo implode(',', $file_uploadFiles); ?>" />				<div class="fltlft" <?php if (!JFactory::getUser()->authorise('core.admin','research')): ?> style="display:none;" <?php endif; ?> >
                <?php /*echo JHtml::_('sliders.start', 'permissions-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
                <?php echo JHtml::_('sliders.panel', JText::_('ACL Configuration'), 'access-rules'); ?>
                <fieldset class="panelform">
                    <?php echo $this->form->getLabel('rules'); ?>
                    <?php echo $this->form->getInput('rules'); ?>
                </fieldset>
                <?php echo JHtml::_('sliders.end'); */?>
            </div>
				<?php if (!JFactory::getUser()->authorise('core.admin','research')): ?>
                <script type="text/javascript">
                    jQuery.noConflict();
                    jQuery('.tab-pane select').each(function(){
                       var option_selected = jQuery(this).find(':selected');
                       var input = document.createElement("input");
                       input.setAttribute("type", "hidden");
                       input.setAttribute("name", jQuery(this).attr('name'));
                       input.setAttribute("value", option_selected.val());
                       document.getElementById("form-publication").appendChild(input);
                    });
                </script>
             <?php endif; ?>
			<div class="control-group">
				<div class="controls">

					<?php if ($this->canSave): ?>
						<button type="submit" class="validate btn btn-primary">
							<?php echo JText::_('JSUBMIT'); ?>
						</button>
					<?php endif; ?>
					<a class="btn"
					   href="<?php echo JRoute::_('index.php?option=com_research&task=publicationform.cancel'); ?>"
					   title="<?php echo JText::_('JCANCEL'); ?>">
						<?php echo JText::_('JCANCEL'); ?>
					</a>
				</div>
			</div>

			<input type="hidden" name="option" value="com_research"/>
			<input type="hidden" name="task"
				   value="publicationform.save"/>
			<?php echo JHtml::_('form.token'); ?>
		</form>
	<?php endif; ?>
</div>
