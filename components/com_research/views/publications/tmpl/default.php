<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Research
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_research') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'publicationform.xml');
$canEdit    = $user->authorise('core.edit', 'com_research') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'publicationform.xml');
$canCheckin = $user->authorise('core.manage', 'com_research');
$canChange  = $user->authorise('core.edit.state', 'com_research');
$canDelete  = $user->authorise('core.delete', 'com_research');

$app = JFactory::getApplication();

    $mycom_params = $app->getParams('com_research');

    // var_dump($mycom_params);
?>

<div class="col-md-3 documents-sidebar hidden">
	<?php echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__)); ?>
	<?php
	  $position = 'upcoming_events'; //position of module in the backend
	  $modules =& JModuleHelper::getModules($position);
	  if (!empty($modules)) { ?>
	    <div class="ibox float-e-margins">
	<br />
	      <?php
	      foreach ($modules as $module) {
	        echo JModuleHelper::renderModule($module);
	      }
	       ?>
	       <div class="clearfix"></div>
	       <br/>
	       </br/>
	   </div>
	  <?php  }?>
</div>

	<?php //echo $mycom_params->get('menu-meta_description');?>

<div class="col-md-9">
<form action="<?php echo htmlspecialchars(JUri::getInstance()->toString()); ?>" method="post"
      name="adminForm" id="adminForm">
      <!-- <br /> -->
      <?php echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__)); ?>

	<table class="table table-striped" id="publicationList">
		<thead>
		<tr>
			<?php if (isset($this->items[0]->state)): ?>
				<th class="hidden" width="5%">
	<?php echo JHtml::_('grid.sort', 'JPUBLISHED', 'a.state', $listDirn, $listOrder); ?>
</th>
			<?php endif; ?>

							<!-- <th class=''>
				<?php //echo JHtml::_('grid.sort',  'COM_RESEARCH_PUBLICATIONS_ID', 'a.id', $listDirn, $listOrder); ?>
				</th> -->
				<th class=''>
				<?php echo JHtml::_('grid.sort',  'COM_RESEARCH_PUBLICATIONS_TITLE', 'a.title', $listDirn, $listOrder); ?>
				</th>
				<th class=''>
				<?php echo JHtml::_('grid.sort',  'COM_RESEARCH_PUBLICATIONS_TYPE', 'a.type', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_RESEARCH_PUBLICATIONS_SUBJECT', 'a.subject', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_RESEARCH_PUBLICATIONS_CONTRIBUTOR_ADVISOR', 'a.contributor_advisor', $listDirn, $listOrder); ?>
				</th>
				<th class=''>
				<?php echo JHtml::_('grid.sort',  'Author', 'a.contributor_author', $listDirn, $listOrder); ?>
				</th>
				<th class=''>
				<?php echo JHtml::_('grid.sort',  'COM_RESEARCH_PUBLICATIONS_DATE_AVAILABLE', 'a.date_available', $listDirn, $listOrder); ?>
				</th>


							<?php if ($canEdit || $canDelete): ?>
					<th class="center">
				<?php echo JText::_('COM_RESEARCH_PUBLICATIONS_ACTIONS'); ?>
				</th>
				<?php endif; ?>

		</tr>
		</thead>
		<tfoot>
		<tr>
			<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
		</tfoot>
		<tbody>
		<?php foreach ($this->items as $i => $item) : ?>
			<?php $canEdit = $user->authorise('core.edit', 'com_research'); ?>

							<?php if (!$canEdit && $user->authorise('core.edit.own', 'com_research')): ?>
					<?php $canEdit = JFactory::getUser()->id == $item->created_by; ?>
				<?php endif; ?>

			<tr class="row<?php echo $i % 2; ?>">

				<?php if (isset($this->items[0]->state)) : ?>
					<?php $class = ($canChange) ? 'active' : 'disabled'; ?>
					<td class="center hidden">
	<a class="btn btn-micro <?php echo $class; ?>" href="<?php echo ($canChange) ? JRoute::_('index.php?option=com_research&task=publication.publish&id=' . $item->id . '&state=' . (($item->state + 1) % 2), false, 2) : '#'; ?>">
	<?php if ($item->state == 1): ?>
		<i class="icon-publish"></i>
	<?php else: ?>
		<i class="icon-unpublish"></i>
	<?php endif; ?>
	</a>
</td>
				<?php endif; ?>

								<!-- <td>

					<?php //echo $item->id; ?>
				</td> -->
				<td>
					<a href="<?php echo JRoute::_('index.php?option=com_research&view=publication&id='.(int) $item->id); ?>" class="hasPopover" title="" data-content="Read more on <?php echo $this->escape($item->title); ?>" data-placement="top" data-original-title="<?php echo $item->subject; ?>">
				<?php echo $this->escape($item->title); ?></a>
				</td>
				<td>

					<?php echo $item->type; ?>
				</td>
				<td class="hidden">

					<?php echo $item->subject; ?>
				</td>
				<td class="hidden">
				<?php if (isset($item->checked_out) && $item->checked_out) : ?>
					<?php echo JHtml::_('jgrid.checkedout', $i, $item->uEditor, $item->checked_out_time, 'publications.', $canCheckin); ?>
				<?php endif; ?>
			<!-- 	<a href="<?php //echo JRoute::_('index.php?option=com_research&view=publication&id='.(int) $item->id); ?>" class="hasPopover" title="" data-content="Select to sort by this column" data-placement="top" data-original-title="<?php //echo $item->subject; ?>"> -->
				<?php echo $this->escape($item->contributor_advisor); ?><!-- </a> -->
				</td>
				<td>
					<?php echo $item->contributor_author; ?>
				</td>
				<td>
					<?php $date = $item->date_available;

          echo date('l jS \of F Y' ,strtotime($date)); ?>
				</td>
				<td class="hidden">
					<?php echo $item->subject; ?>
				</td>
				<td class="hidden">
					<a href="<?php echo JRoute::_('index.php?option=com_research&view=publication&id='.(int) $item->id); ?>" class="hasPopover" title="" data-content="Select to sort by this column" data-placement="top" data-original-title="<?php echo $item->subject; ?>">
				<?php echo $this->escape($item->title); ?></a>
				</td>


								<?php if ($canEdit || $canDelete): ?>
					<td class="center">
						<?php if ($canEdit): ?>
							<a href="<?php echo JRoute::_('index.php?option=com_research&task=publicationform.edit&id=' . $item->id, false, 2); ?>" class="btn btn-mini" type="button"><i class="icon-edit" ></i></a>
						<?php endif; ?>
						<?php if ($canDelete): ?>
							<a href="<?php echo JRoute::_('index.php?option=com_research&task=publicationform.remove&id=' . $item->id, false, 2); ?>" class="btn btn-mini delete-button" type="button"><i class="icon-trash" ></i></a>
						<?php endif; ?>
					</td>
				<?php endif; ?>

			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

	<?php if ($canCreate) : ?>
		<a href="<?php echo JRoute::_('index.php?option=com_research&task=publicationform.edit&id=0', false, 0); ?>"
		   class="btn btn-success btn-small"><i
				class="icon-plus"></i>
			<?php echo JText::_('COM_RESEARCH_ADD_ITEM'); ?></a>
	<?php endif; ?>

	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
	<?php echo JHtml::_('form.token'); ?>
</form>

<?php if($canDelete) : ?>
<script type="text/javascript">

	jQuery(document).ready(function () {
		jQuery('.delete-button').click(deleteItem);
	});

	function deleteItem() {

		if (!confirm("<?php echo JText::_('COM_RESEARCH_DELETE_MESSAGE'); ?>")) {
			return false;
		}
	}
</script>
<?php endif; ?>

</div>

<div class="col-md-3">
	<?php
	  $position = 'publications'; //position of module in the backend
	  $modules =& JModuleHelper::getModules($position);
	  if (!empty($modules)) { ?>
	    <div class="ibox float-e-margins">
	      <?php
	      foreach ($modules as $module) {
	        echo JModuleHelper::renderModule($module);
	      }
	       ?>
	       <div class="clearfix"></div>
	       <br/>
	   </div>
	  <?php  }?>
	</div>
