<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Research
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_research.' . $this->item->id);

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_research' . $this->item->id))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
// var_dump($this->item);
?>

<!-- CUstom view -->
<div class="container-fluid">
	<div class="row">

		<div class="col-md-12">
			<div class="item-summary-view-metadata">
				<div class="clearfix">
					<?php
					$previous = "javascript:history.go(-1)";
					if(isset($_SERVER['HTTP_REFERER'])) {
					    $previous = $_SERVER['HTTP_REFERER'];
					}
					?>
                <a class="back-to-category" href="<?= $previous ?>">
                    <i class="fa fa-arrow-circle-o-left"></i> Back to other <?php echo $this->item->type; ?>               </a>
            </div>

            <!-- <header>
		        <div class="research-type">Document type: <?php //echo $this->item->title; ?></div>
		        <h1><?php //echo $this->item->title; ?></h1>
		    </header> -->

		    <div class="item-view-head">
			<h1><?php echo $this->item->title; ?></h1>
			<div class="simple-item-view-authors">
			<span class="bold">Author:</span>
			<span><?php echo $this->item->contributor_author; ?></span>
			</div>
			</div>

			<div class="simple-item-view-other hidden">
			<!-- <span class="bold">Subject:</span> -->
			<!-- <span xmlns:i18n="#"><?php //echo $this->item->subject; ?></span> -->
			</div>

			<!-- <div id="item-summary-view-citable-uri">
			<span class="bold">Citable URI:  
			                  <a href="<?php //echo $this->item->identifier_uri; ?>" target="_blank"><?php //echo $this->item->identifier_uri; ?></a>
			</span>
			</div> -->
			<?php if (!empty($this->item->external_uri)){ ?>
			<div id="item-summary-view-external-uri">
			<span class="bold">External URI:  
			                  <a href="<?php echo $this->item->external_uri; ?>" target="_blank"><?php echo $this->item->external_uri; ?></a>
			</span>
			</div>
			<?php } ?>
			<div class="simple-item-view-other">
			<span class="bold">Publisher:</span>
			<span xmlns:i18n="#"><?php echo $this->item->publisher; ?></span>
			</div>

			<div class="simple-item-view-other">
			<span class="bold">Date Issued:</span>
			<span xmlns:i18n="#"><?php 
			$date = $this->item->date_available;
			// echo(date("F d, Y h:i:s", $date));
			// echo $this->item->date_available; 
			echo date('l jS \of F Y' ,strtotime($date));
			?></span>
			</div>
			<br />
			<div class="simple-item-view-description">
			<span class="bold">Abstract:</span>
			<div xmlns:i18n="#">
				<?php echo $this->item->description; ?>
			</div>
			</div>

			
			<div class="simple-item-view-other">
			<span class="bold">Citation:</span>
			<span xmlns:i18n="#"><?php echo $this->item->contributor_advisor; ?></span>
			</div>

			<div class="simple-item-view-other hidden">
			<span class="bold">Version:</span>
			<span xmlns:i18n="http://apache.org/cocoon/i18n/2.1">Author's final manuscript</span>
			</div>

			<div class="simple-item-view-other hidden">
			<span class="bold">Terms of Use:</span>
			<span xmlns:i18n="http://apache.org/cocoon/i18n/2.1">Creative Commons Attribution-Noncommercial-Share Alike 3.0</span>
			</div>

			<!-- <div class="simple-item-view-other">
			<span class="bold">Detailed Terms:</span>
			<span xmlns:i18n="http://apache.org/cocoon/i18n/2.1">
			<a href="#"><?php //echo $this->item->rights; ?></a>
			</span>
			</div> -->

		    <ul class="document-meta hidden">
					                     <li>
                            <dl>
                                <dt>Publisher</dt>
                                <dd><a href="#" target="_blank"><?php echo $this->item->created_by_name; ?></a></dd>
                            </dl>
                        </li>
					    </ul>
						<div class="clearfix"></div>
						<br />
						<?php
									foreach ((array) $this->item->file_upload as $singleFile) : 
										if (!is_array($singleFile)) : 
											$uploadPath = 'research' . DIRECTORY_SEPARATOR . $singleFile;
											 echo '<a class="btn btn-primary btn-lg btn-download-document" href="' . JRoute::_(JUri::root() . $uploadPath, false) . '" target="_blank"><i class="fa fa-download"></i> Download this Document</a> ';
										endif;
									endforeach;
							?>
							<div class="clearfix"></div>
							<a class="a2a_button_facebook"></a>
							<a class="a2a_button_twitter"></a>
							<a class="a2a_button_pinterest"></a>
							<br />
							
<iframe src="http://docs.google.com/gview?url=http://dialogkenya.info/research/<?php echo $this->item->file_upload;?>&embedded=true" style="width:100%; height:500px;" frameborder="0"></iframe>
			<!-- <a class="btn btn-primary btn-lg btn-download-document" href="http://www.genocideresearchhub.org.rw/app/uploads/2017/09/Benda-WP001-Final.pdf" target="_blank" data-event-label="Youth Connekt Dialogue: Unwanted Legacies, Responsibility and Nation-building in Rwanda" data-event-value="1599">
                <i class="fa fa-download"></i> Download this Document
            </a> -->

            <div class="document hidden">
            	<br/>
            	<?php echo nl2br($this->item->note); ?>
            </div>
			</div>
		</div>
	</div>
</div>
<!-- End custom view -->

<?php if($canEdit && $this->item->checked_out == 0): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_research&task=publication.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_RESEARCH_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete','com_research.publication.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_RESEARCH_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_RESEARCH_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_RESEARCH_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_research&task=publication.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_RESEARCH_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>


        <br /><br />
        <?php $uri = JFactory::getURI(); 
                    $pageURL = $uri->toString(); 
                    // $pageURL = $uri->var->id; 
                    // echo '<pre>';
                    // var_dump($uri);
                    // echo '<pre/>';
                    $page_id = $this->item->title; 
                    $cat_id = $this->item->type; 

                    ?>
					<div id="disqus_thread"></div>
                        <script>
                        
                        var disqus_config = function () {
                        this.page.url = '<?php echo $pageURL; ?>';  
                        this.page.identifier = '<?php echo $page_id;?>'; 
                        this.page.title = '<?php echo $page_id;?>'; 
                        this.page.category = '<?php echo $cat_id;?>'; 
                        console.log(this.page);
                        };

                        (function() { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = 'https://dialogkenya.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                        })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by DialogKenya.</a></noscript>