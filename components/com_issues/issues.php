<?php
/**
 * @version    CVS: 2.0.1
 * @package    Com_Issues
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Issues', JPATH_COMPONENT);
JLoader::register('IssuesController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Issues');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
