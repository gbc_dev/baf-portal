<?php
/**
 * @version    CVS: 2.0.1
 * @package    Com_Issues
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

// Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_issues', JPATH_SITE);
$doc = JFactory::getDocument();
$doc->addScript(JUri::base() . '/media/com_issues/js/form.js');

$user    = JFactory::getUser();
$canEdit = IssuesHelpersIssues::canUserEdit($this->item, $user);


?>

<div class="issue-edit front-end-edit">
	<?php if (!$canEdit) : ?>
		<h3>
			<?php throw new Exception(JText::_('COM_ISSUES_ERROR_MESSAGE_NOT_AUTHORISED'), 403); ?>
		</h3>
	<?php else : ?>
		<?php if (!empty($this->item->id)): ?>
			<h1><?php echo JText::sprintf('COM_ISSUES_EDIT_ITEM_TITLE', $this->item->id); ?></h1>
		<?php else: ?>
			<h1><?php echo JText::_('COM_ISSUES_ADD_ITEM_TITLE'); ?></h1>
		<?php endif; ?>

		<form id="form-issue"
			  action="<?php echo JRoute::_('index.php?option=com_issues&task=issue.save'); ?>"
			  method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
			
				<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php echo $this->form->getInput('created_by'); ?>
				<?php echo $this->form->getInput('modified_by'); ?>
				<?php echo $this->form->renderField('issue_title'); ?>
				<?php echo $this->form->renderField('category'); ?>
				<?php echo $this->form->renderField('description'); ?>
				<?php echo $this->form->renderField('attachments'); ?>
				<?php echo $this->form->renderField('attachment_found'); ?>
				<?php echo $this->form->renderField('file_name'); ?>
				<?php echo $this->form->renderField('file_upload'); ?>

				<?php if (!empty($this->item->file_upload)) : ?>
					<?php $file_uploadFiles = array(); ?>
					<?php foreach ((array)$this->item->file_upload as $fileSingle) : ?>
						<?php if (!is_array($fileSingle)) : ?>
							<a href="<?php echo JRoute::_(JUri::root() . 'docs' . DIRECTORY_SEPARATOR . $fileSingle, false);?>"><?php echo $fileSingle; ?></a> | 
							<?php $file_uploadFiles[] = $fileSingle; ?>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>
				<input type="hidden" name="jform[file_upload_hidden]" id="jform_file_upload_hidden" value="<?php echo implode(',', $file_uploadFiles); ?>" />

	<?php echo $this->form->renderField('manuals'); ?>

	<?php foreach((array)$this->item->manuals as $value): ?>
		<?php if(!is_array($value)): ?>
			<input type="hidden" class="manuals" name="jform[manualshidden][<?php echo $value; ?>]" value="<?php echo $value; ?>" />';
		<?php endif; ?>
	<?php endforeach; ?>
	<?php echo $this->form->renderField('research'); ?>

	<?php foreach((array)$this->item->research as $value): ?>
		<?php if(!is_array($value)): ?>
			<input type="hidden" class="research" name="jform[researchhidden][<?php echo $value; ?>]" value="<?php echo $value; ?>" />';
		<?php endif; ?>
	<?php endforeach; ?>
	<?php echo $this->form->renderField('first_key_player'); ?>

	<?php foreach((array)$this->item->first_key_player as $value): ?>
		<?php if(!is_array($value)): ?>
			<input type="hidden" class="first_key_player" name="jform[first_key_playerhidden][<?php echo $value; ?>]" value="<?php echo $value; ?>" />';
		<?php endif; ?>
	<?php endforeach; ?>
	<?php echo $this->form->renderField('issue_players'); ?>

	<input type="hidden" name="jform[published]" value="<?php echo $this->item->published; ?>" />

	<input type="hidden" name="jform[created]" value="<?php echo $this->item->created; ?>" />

	<input type="hidden" name="jform[modified]" value="<?php echo $this->item->modified; ?>" />

	<?php echo $this->form->renderField('publish_up'); ?>

	<?php echo $this->form->renderField('publish_down'); ?>

	<?php echo $this->form->renderField('access'); ?>
				<div class="fltlft" <?php if (!JFactory::getUser()->authorise('core.admin','issues')): ?> style="display:none;" <?php endif; ?> >
                <?php echo JHtml::_('sliders.start', 'permissions-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
                <?php echo JHtml::_('sliders.panel', JText::_('ACL Configuration'), 'access-rules'); ?>
                <fieldset class="panelform">
                    <?php echo $this->form->getLabel('rules'); ?>
                    <?php echo $this->form->getInput('rules'); ?>
                </fieldset>
                <?php echo JHtml::_('sliders.end'); ?>
            </div>
				<?php if (!JFactory::getUser()->authorise('core.admin','issues')): ?>
                <script type="text/javascript">
                    jQuery.noConflict();
                    jQuery('.tab-pane select').each(function(){
                       var option_selected = jQuery(this).find(':selected');
                       var input = document.createElement("input");
                       input.setAttribute("type", "hidden");
                       input.setAttribute("name", jQuery(this).attr('name'));
                       input.setAttribute("value", option_selected.val());
                       document.getElementById("form-issue").appendChild(input);
                    });
                </script>
             <?php endif; ?>
			<div class="control-group">
				<div class="controls">

					<?php if ($this->canSave): ?>
						<button type="submit" class="validate btn btn-primary">
							<?php echo JText::_('JSUBMIT'); ?>
						</button>
					<?php endif; ?>
					<a class="btn"
					   href="<?php echo JRoute::_('index.php?option=com_issues&task=issueform.cancel'); ?>"
					   title="<?php echo JText::_('JCANCEL'); ?>">
						<?php echo JText::_('JCANCEL'); ?>
					</a>
				</div>
			</div>

			<input type="hidden" name="option" value="com_issues"/>
			<input type="hidden" name="task"
				   value="issueform.save"/>
			<?php echo JHtml::_('form.token'); ?>
		</form>
	<?php endif; ?>
</div>
