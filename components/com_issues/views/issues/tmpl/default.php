<?php
/**
 * @version    CVS: 2.0.1
 * @package    Com_Issues
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_issues') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'issueform.xml');
$canEdit    = $user->authorise('core.edit', 'com_issues') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'issueform.xml');
$canCheckin = $user->authorise('core.manage', 'com_issues');
$canChange  = $user->authorise('core.edit.state', 'com_issues');
$canDelete  = $user->authorise('core.delete', 'com_issues');
$page_desc = $this->params->get('menu-meta_description');
?>

<?php echo $page_desc;?>
<hr>
<form action="<?php echo htmlspecialchars(JUri::getInstance()->toString()); ?>" method="post"
      name="adminForm" id="adminForm">

	<?php echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__)); ?>
	<table class="table table-striped" id="issueList">
		<thead>
		<tr>
			<?php if (isset($this->items[0]->state)): ?>
				<th class="hidden" width="5%">
	<?php echo JHtml::_('grid.sort', 'JPUBLISHED', 'a.state', $listDirn, $listOrder); ?>
</th>
			<?php endif; ?>

							<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_ID', 'a.id', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_CREATED_BY', 'a.created_by', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_MODIFIED_BY', 'a.modified_by', $listDirn, $listOrder); ?>
				</th>
				<th class=''>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_ISSUE_TITLE', 'a.issue_title', $listDirn, $listOrder); ?>
				</th>
				<th class=''>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_CATEGORY', 'a.category', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_DESCRIPTION', 'a.description', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_ATTACHMENTS', 'a.attachments', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_FILE_UPLOAD', 'a.file_upload', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_MANUALS', 'a.manuals', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_RESEARCH', 'a.research', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_FIRST_KEY_PLAYER', 'a.first_key_player', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_ISSUE_PLAYERS', 'a.issue_players', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_PUBLISHED', 'a.published', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_CREATED', 'a.created', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_MODIFIED', 'a.modified', $listDirn, $listOrder); ?>
				</th>


							<?php if ($canEdit || $canDelete): ?>
					<th class="center">
				<?php echo JText::_('COM_ISSUES_ISSUES_ACTIONS'); ?>
				</th>
				<?php endif; ?>

		</tr>
		</thead>
		<tfoot>
		<tr>
			<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
		</tfoot>
		<tbody>
		<?php foreach ($this->items as $i => $item) : ?>
			<?php $canEdit = $user->authorise('core.edit', 'com_issues'); ?>

							<?php if (!$canEdit && $user->authorise('core.edit.own', 'com_issues')): ?>
					<?php $canEdit = JFactory::getUser()->id == $item->created_by; ?>
				<?php endif; ?>

			<tr class="row<?php echo $i % 2; ?>">

				<?php if (isset($this->items[0]->state)) : ?>
					<?php $class = ($canChange) ? 'active' : 'disabled'; ?>
					<td class="center hidden">
	<a class="btn btn-micro <?php echo $class; ?>" href="<?php echo ($canChange) ? JRoute::_('index.php?option=com_issues&task=issue.publish&id=' . $item->id . '&state=' . (($item->state + 1) % 2), false, 2) : '#'; ?>">
	<?php if ($item->state == 1): ?>
		<i class="icon-publish"></i>
	<?php else: ?>
		<i class="icon-unpublish"></i>
	<?php endif; ?>
	</a>
</td>
				<?php endif; ?>

								<td class="hidden">

					<?php echo $item->id; ?>
				</td>
				<td class="hidden">

							<?php echo JFactory::getUser($item->created_by)->name; ?>				</td>
				<td class="hidden">

							<?php echo JFactory::getUser($item->modified_by)->name; ?>				</td>
				<td>
				<?php if (isset($item->checked_out) && $item->checked_out) : ?>
					<?php echo JHtml::_('jgrid.checkedout', $i, $item->uEditor, $item->checked_out_time, 'issues.', $canCheckin); ?>
				<?php endif; ?>
				<a href="<?php echo JRoute::_('index.php?option=com_issues&view=issue&id='.(int) $item->id); ?>">
				<?php echo $this->escape($item->issue_title); ?></a>
				</td>
				<td>
					<a href="index.php?option=com_issues&view=cats"><?php echo $item->category; ?></a>
				</td>
				<td class="hidden">
					<?php echo $item->description; ?>
				</td>
				<td class="hidden">
					<?php echo $item->attachments; ?>
				</td>
				<td class="hidden">
					<?php
						if (!empty($item->file_upload)) :
							$file_uploadArr = (array) explode(',', $item->file_upload);
							foreach ($file_uploadArr as $singleFile) :
								if (!is_array($singleFile)) :
									$uploadPath = 'docs' . DIRECTORY_SEPARATOR . $singleFile;
									echo '<a href="' . JRoute::_(JUri::root() . $uploadPath, false) . '" target="_blank" title="See the file_upload">' . $singleFile . '</a> ';
								endif;
							endforeach;
						else:
							echo $item->file_upload;
						endif; ?>				</td>
				<td class="hidden">
					<?php echo $item->manuals; ?>
				</td>
				<td class="hidden">
					<?php echo $item->research; ?>
				</td>
				<td class="hidden">
					<?php echo $item->first_key_player; ?>
				</td>
				<td class="hidden">
					<?php echo $item->issue_players; ?>
				</td>
				<td class="hidden">
					<?php echo $item->published; ?>
				</td>
				<td class="hidden">
					<?php echo $item->created; ?>
				</td>
				<td class="hidden">
					<?php echo $item->modified; ?>
				</td>


								<?php if ($canEdit || $canDelete): ?>
					<td class="center">
						<?php if ($canEdit): ?>
							<a href="<?php echo JRoute::_('index.php?option=com_issues&task=issueform.edit&id=' . $item->id, false, 2); ?>" class="btn btn-mini" type="button"><i class="icon-edit" ></i></a>
						<?php endif; ?>
						<?php if ($canDelete): ?>
							<a href="<?php echo JRoute::_('index.php?option=com_issues&task=issueform.remove&id=' . $item->id, false, 2); ?>" class="btn btn-mini delete-button" type="button"><i class="icon-trash" ></i></a>
						<?php endif; ?>
					</td>
				<?php endif; ?>

			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

	<?php if ($canCreate) : ?>
		<a href="<?php echo JRoute::_('index.php?option=com_issues&task=issueform.edit&id=0', false, 0); ?>"
		   class="btn btn-success btn-small"><i
				class="icon-plus"></i>
			<?php echo JText::_('COM_ISSUES_ADD_ITEM'); ?></a>
	<?php endif; ?>

	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
	<?php echo JHtml::_('form.token'); ?>
</form>

<?php if($canDelete) : ?>
<script type="text/javascript">

	jQuery(document).ready(function () {
		jQuery('.delete-button').click(deleteItem);
	});

	function deleteItem() {

		if (!confirm("<?php echo JText::_('COM_ISSUES_DELETE_MESSAGE'); ?>")) {
			return false;
		}
	}
</script>
<?php endif; ?>
