<?php
/**
 * @version    CVS: 2.0.1
 * @package    Com_Issues
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
$cat = $this->item;
// print_r($cat['1']);
$canEdit = JFactory::getUser()->authorise('core.edit', 'com_issues.' . $cat['0']);

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_issues' . $cat['0']))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_issues') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'issueform.xml');
$canEdit    = $user->authorise('core.edit', 'com_issues') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'issueform.xml');
$canCheckin = $user->authorise('core.manage', 'com_issues');
$canChange  = $user->authorise('core.edit.state', 'com_issues');
$canDelete  = $user->authorise('core.delete', 'com_issues');
$page_desc = $this->params->get('menu-meta_description');
?>

<div class="kfrontend">
		<h3 class="btn-toolbar pull-right">
							<button class="btn btn-default btn-small fa" type="button" data-toggle="collapse" data-target="#section5" aria-expanded="false" aria-controls="section5"></button>
					</h3>

		<h3 class="btn-link">
			<a href="#" title="View Category '<?php echo $cat['1'];?>'" class="hasTooltip"><?php echo $cat['1'];?></a>
			<small class="hidden nowrap">(2 topics)</small>
		</h3>

		<div class="row-fluid collapse in section section" id="section5">
			<table class="table table-bordered">
									<thead class="hidden-xs">
					<tr>
						<td colspan="3">
							<div class="header-desc"><?php echo $cat['2'];?> </div>
						</td>
					</tr>
					</thead>



			</table>
		</div>
	</div>

<div class="item_fields">

	<!-- <table class="table">


	</table> -->
	<form action="<?php echo htmlspecialchars(JUri::getInstance()->toString()); ?>" method="post"
	      name="adminForm" id="adminForm">

		<?php echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__)); ?>
		<table class="table table-striped" id="issueList">
			<thead>
			<tr>
				<?php if (isset($this->items[0]->state)): ?>
					<th class="hidden" width="5%">
		<?php echo JHtml::_('grid.sort', 'JPUBLISHED', 'a.state', $listDirn, $listOrder); ?>
	</th>
				<?php endif; ?>


					<th class=''>
					<?php echo JHtml::_('grid.sort',  'COM_ISSUES_ISSUES_ISSUE_TITLE', 'a.issue_title', $listDirn, $listOrder); ?>
					</th>



								<?php if ($canEdit || $canDelete): ?>
						<th class="center hidden">
					<?php echo JText::_('COM_ISSUES_ISSUES_ACTIONS'); ?>
					</th>
					<?php endif; ?>

			</tr>
			</thead>
			<tfoot>
			<tr>
				<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
					<?php //echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php
			$this->items = $this->children;
			foreach ($this->items as $i => $item) : ?>
				<?php $canEdit = $user->authorise('core.edit', 'com_issues'); ?>

								<?php if (!$canEdit && $user->authorise('core.edit.own', 'com_issues')): ?>
						<?php $canEdit = JFactory::getUser()->id == $item->created_by; ?>
					<?php endif; ?>

				<tr class="row<?php echo $i % 2; ?>">


					<td>
					<?php if (isset($item->checked_out) && $item->checked_out) : ?>
						<?php echo JHtml::_('jgrid.checkedout', $i, $item->uEditor, $item->checked_out_time, 'issues.', $canCheckin); ?>
					<?php endif; ?>
					<a href="<?php echo JRoute::_('index.php?option=com_issues&view=issue&id='.(int) $item['0']); ?>">
					<?php
					// var_dump($item);
					echo $item['3']; ?></a>
					</td>


				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>

		<input type="hidden" name="task" value=""/>
		<input type="hidden" name="boxchecked" value="0"/>
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
		<?php echo JHtml::_('form.token'); ?>
	</form>

</div>
