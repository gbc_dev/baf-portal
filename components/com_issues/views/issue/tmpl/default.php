<?php
/**
 * @version    CVS: 2.0.1
 * @package    Com_Issues
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_issues.' . $this->item->id);

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_issues' . $this->item->id)) {
    $canEdit = JFactory::getUser()->id == $this->item->created_by;
}
?>

<div class="row">
<div class="col-md-9">
	<h3><?php echo $this->item->issue_title; ?></h3>
	<hr>
	<div class="item_fields">
			<div class="content_Desc">
				<?php echo $this->item->description; ?>
				<hr>
			</div>
			<div class="cat_Desc">
				<div class="col-md-1">
				<i class="fa fa-folder fa-6" aria-hidden="true"></i>
				</div>
				<div class="col-md-11">
					<?php //echo $this->category->displayField('headerdesc');
          // $post = JRequest::get('post');
// print_r($post);

          ?>
          <form action="" method="post" name="adminForm">
          <!-- <form action="index.php?option=com_issues&view=issues&Itemid=278" method="post"> -->
           <!--  <input type="hidden" name="option" value="com_issues" />
          <input type="hidden" name="task" value="" /> -->
          <input type="hidden" name="option" value="com_issues" />
<input type="hidden" name="view" value="issues" />
<input type="hidden" name="task" value="" />
<input type="hidden" value="issues" name="controller">
<!-- <input type="submit" /> -->

<a href="index.php?option=com_issues&view=issues&Itemid=278" type="submit">
  <h4><?php echo $this->item->category;?></h4>
</a>


					<!-- <a href="index.php?option=com_issues&view=issues&Itemid=278" type="submit"><h4><?php //echo $this->item->category;?></h4></a> -->

          <!-- <input type="submit" name="Submit" value="Submit" />  -->
          


        </form>
				</div>
			</div>
			<div class="afterContent row">
				<div class="col-md-12">
					<?php
                      $position = 'issue-share'; //position of module in the backend
                      $modules =& JModuleHelper::getModules($position);
                      if (!empty($modules)) {
                          ?>
					    <div class="">
					      <?php
                          foreach ($modules as $module) {
                              echo JModuleHelper::renderModule($module);
                          } ?>
					       <div class="clearfix"></div>
					       <br/>
					   </div>
					  <?php
                      }?>
				</div>
				<div class="col-md-12">
					<?php $uri = JFactory::getURI();
                    $pageURL = $uri->toString();
                    $page_id = $this->item->issue_title; ?>
					<div id="disqus_thread"></div>
						<script>

						var disqus_config = function () {
						this.page.url = '<?php echo $pageURL; ?>';
						this.page.identifier = '<?php echo $page_id;?>';
						};

						(function() { // DON'T EDIT BELOW THIS LINE
						var d = document, s = d.createElement('script');
						s.src = 'https://dialogkenya.disqus.com/embed.js';
						s.setAttribute('data-timestamp', +new Date());
						(d.head || d.body).appendChild(s);
						})();
						</script>
						<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by DialogKenya.</a></noscript>

				</div>
			</div>
			<br />
		</div>
	</div>
<div class="col-md-3">
	<br>
		<?php
        $newtext = $this->item->description;
        // var_dump($newtext);
        //Create a new DOM document
        $dom = new DOMDocument;

        //Parse the HTML. The @ is used to suppress any parsing errors
        //that will be thrown if the $html string isn't valid XHTML.
        @$dom->loadHTML($newtext);

        //Get all links. You could also use any other tag name here,
        //like 'img' or 'table', to extract other tags.
        $links = $dom->getElementsByTagName('a');


        //Iterate over the extracted links and display their URLs
        // foreach ($links as $link){
            // echo '<pre>';
            // var_dump($links);
            // var_dump($newtext);
            // echo '<pre/>';
        //     //Extract and show the "href" attribute.
        //     // echo $link->nodeValue;
        //     // echo ' '. $link->getAttribute('href');
        // }

    // if (($links >= '1') && $issueLinks <= '1' {
        // if (($links >= '1')  {
    // if (($links !== '')) {
     if (!empty($links)) {
         ?>
		<div class="issueLinks">
				<h2><i class="fa fa-link" aria-hidden="true"></i> &nbsp;Issue Links</h2>
				<ul>
					<?php foreach ($links as $link) {
             // var_dump($link);
             $filename =  $link->getAttribute('href');
             $ext = pathinfo($filename, PATHINFO_EXTENSION);
             // echo $ext;
             $arrayName = array($ext);

             //foreach loop
             foreach ($arrayName as $key => $value) {
                 //condition
                            // var_dump($value);
                            if ($value != 'doc' && $value != 'docx' && $value != 'pdf') {  //1. Condition fulfilled
                                $newArray[ ] = $value;
                                //↑ Put '$key' there, if you want to keep the original keys
                                //Result array is: $newArray
                                // var_dump($value); ?>
							    <li><a href="<?php echo $link->getAttribute('href'); ?>" target="_blank"><i class="fa fa-link" aria-hidden="true"></i> <?php echo $link->textContent; ?></a> </li>
							<?php
                            }
             }

             // if (in_array("doc", $arrayName) || in_array("docx", $arrayName) || in_array("pdf", $arrayName)) {
                            // var_dump($link);
                     ?>
						<!-- <li><i class="fa fa-link" aria-hidden="true"></i> <a href="<?php //echo $link->getAttribute('href');?>" target="_blank"><?php //echo $link->textContent;?></a> </li> -->
					<?php //}
                     //else { echo 'nothing';
         } ?>
				</ul>
			</div>
			<div class="clearfix"></div>
	<?php
     } else {
         echo 'nothing to show';
     }

// }
    ?>
	<br>
	<div class="issueLinks">
				<h2><i class="fa fa-folder" aria-hidden="true"></i> &nbsp;Issue Documents</h2>
				<?php
                    $newtext2 = $this->item->description;
                    // var_dump($newtext);
                    //Create a new DOM document
                    $dom = new DOMDocument;

                    //Parse the HTML. The @ is used to suppress any parsing errors
                    //that will be thrown if the $html string isn't valid XHTML.
                    @$dom->loadHTML($newtext2);

                    //Get all links. You could also use any other tag name here,
                    //like 'img' or 'table', to extract other tags.
                    $links2 = $dom->getElementsByTagName('a');
                    // var_dump($links2);
                    // $extesnion=f_extension("files/file.pdf"); // returns jpg
                    // echo $extesnion;


                 ?>
				<ul class="issueLinks">
					<?php foreach ($links2 as $link) {
                     // var_dump($link);
                     $filename =  $link->getAttribute('href');
                     $ext = pathinfo($filename, PATHINFO_EXTENSION);
                     // echo $ext;
                     $arrayName = array($ext);

                     if (in_array("doc", $arrayName) || in_array("docx", $arrayName) || in_array("pdf", $arrayName)) {
                        ?>
						<li>
							<a class="doclinks" href="<?php echo $link->getAttribute('href'); ?>" target="_blank"><i class="fa fa-folder" aria-hidden="true"></i> <?php echo $link->textContent; ?></a>
						</li>
					<?php
                     }
                 }  ?>
				</ul>
			</div>

		<!-- <br />
		<p></p>
		<div class="issueLinks">
				<h2><i class="fa fa-youtube-play" aria-hidden="true"></i> &nbsp;Issue Links</h2>
				<ul>
					<li><i class="fa fa-link" aria-hidden="true"></i> <a href="http://docs.kunena.org" target="_blank">
	Mining Bill</a> </li>
					<li><i class="fa fa-link" aria-hidden="true"></i> <a href="http://docs.kunena.org" target="_blank">
	a summary of issues</a> </li>
					<li><i class="fa fa-link" aria-hidden="true"></i> <a href="http://docs.kunena.org" target="_blank">
	critique</a> </li>
	Player 4</a> </li>
									</ul>
			</div> -->

			<!-- Attachements -->
			<br />
			<?php
            // if (!empty($this->item->file_upload)) && (!empty($this->item->attachments)){
            /*if (!empty($this->item->attachments)){
            ?>
            <div class="issueDocs">
                <div class="kattach">
        <h2><i class="fa fa-folder" aria-hidden="true"></i> &nbsp;Issue Documents</h2>
        <ul class="">
                            <!--<li><a href="/dialogkenya/index.php/issue/attachment/6" target="_blank"><i class="fa fa-folder" aria-hidden="true"></i> 0cbacfaee71e988c430b2623b3a13703</a></li>
                <!-<li class="span3 center">
                    <div class="thumbnail">   </div>
                </li>
                    </ul>-->
                    <?php

            // // second file
            // if(!empty($this->item->second_file_upload)){
            // foreach ((array) $this->item->second_file_upload as $singleFile) :
            // 	if (!is_array($singleFile)) :
            // 		$uploadPath = 'docs/issues' . DIRECTORY_SEPARATOR . $singleFile;
            // 		 echo '<li><a href="' . JRoute::_(JUri::root() . $uploadPath, false) . '" target="_blank"><i class="fa fa-folder" aria-hidden="true"></i> ' . $singleFile . '</a></li> ';
            // 	endif;
            // endforeach;
            // };

            // // third file
            // if(!empty($this->item->third_file_upload)){
            // foreach ((array) $this->item->third_file_upload as $singleFile) :
            // 	if (!is_array($singleFile)) :
            // 		$uploadPath = 'docs/issues' . DIRECTORY_SEPARATOR . $singleFile;
            // 		 echo '<li><a href="' . JRoute::_(JUri::root() . $uploadPath, false) . '" target="_blank"><i class="fa fa-folder" aria-hidden="true"></i> ' . $singleFile . '</a></li> ';
            // 	endif;
            // endforeach;
            // }

            ?>
            <?php //echo $this->item->attachments;
                // get the repeatable field value and decode it
                $list_hosts 	 = json_decode( $this->item->attachments,true);
                // var_dump($list_hosts);
                // loop your result
                foreach( $list_hosts as $list_templates_idx => $list_template ) {
                   // do something clever for each of the templates internal_attachment
                    // echo '<br />';
                    // echo $list_template['internal_attachment'];
                    // echo $list_template['internal_file_name'];
                    echo '<li><a href="docs' .$list_template['internal_attachment']. '" target="_blank"><i class="fa fa-folder" aria-hidden="true"></i> ' . $list_template['internal_file_name'] . '</a></li> ';
                }

            if(!empty($this->item->file_upload)){
            foreach ((array) $this->item->file_upload as $singleFile) :
                if (!is_array($singleFile)) :
                    $uploadPath = 'docs/issues' . DIRECTORY_SEPARATOR . $singleFile;
                     echo '<li><a href="' . JRoute::_(JUri::root() . $uploadPath, false) . '" target="_blank"><i class="fa fa-folder" aria-hidden="true"></i> ' . $this->item->file_name . '</a></li> ';
                endif;
            endforeach;
        }
            ?>
        </ul>
    </div>
            </div>
            <br />
            <?php } */?>

			<?php

      // var_dump($this->item->first_key_player);
            if (!empty($this->item->first_key_player)) {
                $keyplayers = (explode(",", $this->item->first_key_player)); ?>
			<div class="issueDocs">
				<h2><i class="fa fa-user" aria-hidden="true"></i> &nbsp;Key Players</h2>
				<ul>
		    	<?php
                //$keyplayers = (explode(" ",$this->item->first_key_player));
                // print_r($keyplayers);
                // echo $this->item->first_key_player;

                // echo '<ul>';
                echo '<li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> ' . implode( '</a></li><li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> ', $keyplayers) . '</a></li>';
                // echo '</ul>';

                // echo $this->item->first_key_player;?>
		    	<?php //echo $this->item->attachments;
                // get the repeatable field value and decode it
          if (!empty($this->item->issue_players)) {
          
              $list_other_players     = json_decode($this->item->issue_players, true);
                // var_dump($this->item);
                // loop your result
                foreach ($list_other_players as $list_templates_idx => $list_template) {
                    // do something clever for each of the templates internal_attachment
                    // echo '<br />';
                    echo '<li><a href="#"><i class="fa fa-user" aria-hidden="true"></i> ';
                    echo $list_template['keyplayers'];
                    echo '</a></li>';
                } 
              }

                ?>
			</ul>
				</div>
				<?php
            }?>
		</div>
</div>


<?php if ($canEdit && $this->item->checked_out == 0): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_issues&task=issue.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_ISSUES_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete', 'com_issues.issue.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_ISSUES_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_ISSUES_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_ISSUES_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_issues&task=issue.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_ISSUES_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>

<script id="dsq-count-scr" src="//dialogkenya.disqus.com/count.js" async></script>
