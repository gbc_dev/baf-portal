<?php
/**
 * @version    CVS: 2.0.1
 * @package    Com_Issues
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$cats = $this->categories;

// foreach ($cats as $key => $value) {
//     # code...
//     echo $value['1'];
//     echo $value['0'];
// }

?>

<table class="table table-bordered">
				<thead class="hidden-xs">
					<tr>
						<td colspan="3">
							<div class="header-desc">This is JoomlaShine forum. Here you are allowed to post topics related to JoomlaShine products.</div>
						</td>
					</tr>
					</thead>

						<tbody><tr>
							<td colspan="2" class="hidden-xs">
								<div class="header-desc">Category</div>
							</td>
						</tr>
						<?php
						foreach ($cats as $key => $value) {
						 ?>
						<tr class="category" id="category<?php echo $value['0'];?>">
							<td class="col-md-1 center hidden-xs">
								<a href="index.php?option=com_issues&view=cat&id=<?php echo $value['0'];?>&Itemid=278" title="View Category 'Joomla templates'"><i class="fa icon-folder fa-big " alt="New Posts" aria-hidden="true"> </i></a>
							</td>
							<td class="col-md-8">
								<div>
									<h3>
										<a href="index.php?option=com_issues&view=cat&id=<?php echo $value['0'];?>&Itemid=278" title="View Category 'Joomla templates'"><?php echo $value['1'];?></a>
										<small class="hidden nowrap">
											(2 topics)
											<span>
												<a href="index.php?option=com_issues&view=cat&id=<?php echo $value['0'];?>Itemid=278" rel="alternate" type="application/rss+xml" data-original-title="get the latest posts directly to your desktop">
														 <i class="fa fa-rss  " title="Get the latest posts directly to your desktop" aria-hidden="true"></i>
											</a>
											</span>
										</small>
									</h3>
								</div>
							<div class="hidden-xs header-desc"><?php echo $value['2'];?></div>
							</td>
						</tr>

					<?php } ?>

			</tbody></table>
