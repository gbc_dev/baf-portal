<?php

/**
 * @version    CVS: 2.0.1
 * @package    Com_Issues
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;

/**
 * Issues model.
 *
 * @since  1.6
 */
class IssuesModelIssue extends JModelItem
{
    public $_item;

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return void
	 *
	 * @since    1.6
	 *
	 */
	protected function populateState()
	{
		$app  = Factory::getApplication('com_issues');
		$user = Factory::getUser();

		// Check published state
		if ((!$user->authorise('core.edit.state', 'com_issues')) && (!$user->authorise('core.edit', 'com_issues')))
		{
			$this->setState('filter.published', 1);
			$this->setState('filter.archived', 2);
		}

		// Load state from the request userState on edit or from the passed variable on default
		if (Factory::getApplication()->input->get('layout') == 'edit')
		{
			$id = Factory::getApplication()->getUserState('com_issues.edit.issue.id');
		}
		else
		{
			$id = Factory::getApplication()->input->get('id');
			Factory::getApplication()->setUserState('com_issues.edit.issue.id', $id);
		}

		$this->setState('issue.id', $id);

		// Load the parameters.
		$params       = $app->getParams();
		$params_array = $params->toArray();

		if (isset($params_array['item_id']))
		{
			$this->setState('issue.id', $params_array['item_id']);
		}

		$this->setState('params', $params);
	}

	/**
	 * Method to get an object.
	 *
	 * @param   integer $id The id of the object to get.
	 *
	 * @return  mixed    Object on success, false on failure.
     *
     * @throws Exception
	 */
	public function getItem($id = null)
	{
		if ($this->_item === null)
		{
			$this->_item = false;

			if (empty($id))
			{
				$id = $this->getState('issue.id');
			}

			// Get a level row instance.
			$table = $this->getTable();

			// Attempt to load the row.
			if ($table->load($id))
			{
				// Check published state.
				if ($published = $this->getState('filter.published'))
				{
					if (isset($table->state) && $table->state != $published)
					{
						throw new Exception(JText::_('COM_ISSUES_ITEM_NOT_LOADED'), 403);
					}
				}

				// Convert the JTable to a clean JObject.
				$properties  = $table->getProperties(1);
				$this->_item = ArrayHelper::toObject($properties, 'JObject');
			}
		}



		if (isset($this->_item->created_by))
		{
			$this->_item->created_by_name = Factory::getUser($this->_item->created_by)->name;
		}

		if (isset($this->_item->modified_by))
		{
			$this->_item->modified_by_name = Factory::getUser($this->_item->modified_by)->name;
		}

		if (isset($this->_item->category) && $this->_item->category != '')
		{
			if (is_object($this->_item->category))
			{
				$this->_item->category = ArrayHelper::fromObject($this->_item->category);
			}

			if (is_array($this->_item->category))
			{
				$this->_item->category = implode(',', $this->_item->category);
			}

			$db    = Factory::getDbo();
			$query = $db->getQuery(true);

			$query
				->select($db->quoteName('title'))
				->from($db->quoteName('#__categories'))
				->where('FIND_IN_SET(' . $db->quoteName('id') . ', ' . $db->quote($this->_item->category) . ')');

			$db->setQuery($query);

			$result = $db->loadColumn();

			$this->_item->category = !empty($result) ? implode(', ', $result) : '';
		}
					$this->_item->attachment_found = JText::_('COM_ISSUES_ISSUES_ATTACHMENT_FOUND_OPTION_' . $this->_item->attachment_found);

		if (isset($this->_item->manuals) && $this->_item->manuals != '')
		{
			if (is_object($this->_item->manuals))
			{
				$this->_item->manuals = ArrayHelper::fromObject($this->_item->manuals);
			}

			$values = (is_array($this->_item->manuals)) ? $this->_item->manuals : explode(',',$this->_item->manuals);

			$textValue = array();

			foreach ($values as $value)
			{
				$db    = Factory::getDbo();
				$query = "SELECT id, file_name, document_name FROM #__manuals_factsheets HAVING id LIKE '" . $value . "'";

				$db->setQuery($query);
				$results = $db->loadObject();

				if ($results)
				{
					$textValue[] = $results->file_name;
				}
			}

			$this->_item->manuals = !empty($textValue) ? implode(', ', $textValue) : $this->_item->manuals;
		}

		if (isset($this->_item->research) && $this->_item->research != '')
		{
			if (is_object($this->_item->research))
			{
				$this->_item->research = ArrayHelper::fromObject($this->_item->research);
			}

			$values = (is_array($this->_item->research)) ? $this->_item->research : explode(',',$this->_item->research);

			$textValue = array();

			foreach ($values as $value)
			{
				$db    = Factory::getDbo();
				$query = "SELECT id, title FROM #__research_publications HAVING id LIKE '" . $value . "'";

				$db->setQuery($query);
				$results = $db->loadObject();

				if ($results)
				{
					$textValue[] = $results->title;
				}
			}

			$this->_item->research = !empty($textValue) ? implode(', ', $textValue) : $this->_item->research;
		}

		if (isset($this->_item->first_key_player) && $this->_item->first_key_player != '')
		{
			if (is_object($this->_item->first_key_player))
			{
				$this->_item->first_key_player = ArrayHelper::fromObject($this->_item->first_key_player);
			}

			$values = (is_array($this->_item->first_key_player)) ? $this->_item->first_key_player : explode(',',$this->_item->first_key_player);

			$textValue = array();
      		$textId = array();

			foreach ($values as $value)
			{
				$db    = Factory::getDbo();
				$query = "SELECT id, metaDesc FROM #__sobipro_object HAVING id = '" . $value . "'";

				$db->setQuery($query);
				$results = $db->loadObject();
        // var_dump($results);

				if ($results)
				{
					$textValue[] = $results->metaDesc;
          			$textId[] = $results->id;
				}
			}

			$this->_item->first_key_player = !empty($textValue) ? implode(', ', $textValue) : $this->_item->first_key_player;
      // $this->_item->id_first_key_player = !empty($textId) ? implode(', ', $textId) : $this->_item->first_key_player;
		}

		return $this->_item;
	}

	/**
	 * Get an instance of JTable class
	 
	 *
	 * @param   string $type   Name of the JTable class to get an instance of.
	 * @param   string $prefix Prefix for the table class name. Optional.
	 * @param   array  $config Array of configuration values for the JTable object. Optional.
	 *
	 * @return  JTable|bool JTable if success, false on failure.
	 */
	public function getTable($type = 'Issue', $prefix = 'IssuesTable', $config = array())
	{
		$this->addTablePath(JPATH_ADMINISTRATOR . '/components/com_issues/tables');

		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Get the id of an item by alias
	 *
	 * @param   string $alias Item alias
	 *
	 * @return  mixed
	 */
	public function getItemIdByAlias($alias)
	{
		$table      = $this->getTable();
		$properties = $table->getProperties();
		$result     = null;

		if (key_exists('alias', $properties))
		{
            $table->load(array('alias' => $alias));
            $result = $table->id;
		}

		return $result;
	}

	/**
	 * Method to check in an item.
	 *
	 * @param   integer $id The id of the row to check out.
	 *
	 * @return  boolean True on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function checkin($id = null)
	{
		// Get the id.
		$id = (!empty($id)) ? $id : (int) $this->getState('issue.id');

		if ($id)
		{
			// Initialise the table
			$table = $this->getTable();

			// Attempt to check the row in.
			if (method_exists($table, 'checkin'))
			{
				if (!$table->checkin($id))
				{
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Method to check out an item for editing.
	 *
	 * @param   integer $id The id of the row to check out.
	 *
	 * @return  boolean True on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function checkout($id = null)
	{
		// Get the user id.
		$id = (!empty($id)) ? $id : (int) $this->getState('issue.id');

		if ($id)
		{
			// Initialise the table
			$table = $this->getTable();

			// Get the current user object.
			$user = Factory::getUser();

			// Attempt to check the row out.
			if (method_exists($table, 'checkout'))
			{
				if (!$table->checkout($user->get('id'), $id))
				{
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Publish the element
	 *
	 * @param   int $id    Item id
	 * @param   int $state Publish state
	 *
	 * @return  boolean
	 */
	public function publish($id, $state)
	{
		$table = $this->getTable();
		$table->load($id);
		$table->state = $state;

		return $table->store();
	}

	/**
	 * Method to delete an item
	 *
	 * @param   int $id Element id
	 *
	 * @return  bool
	 */
	public function delete($id)
	{
		$table = $this->getTable();

		return $table->delete($id);
	}

	// public function getFExt($fn)
	// {
	// 	$str=explode('/',$fn);
	// 	$len=count($str);
	// 	$str2=explode('.',$str[($len-1)]);
	// 	$len2=count($str2);
	// 	$ext=$str2[($len2-1)];
	// 	return $ext;
	// 	}




}
