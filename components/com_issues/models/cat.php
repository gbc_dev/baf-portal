<?php

/**
 * @version    CVS: 2.0.1
 * @package    Com_Issues
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;

/**
 * Issues model.
 *
 * @since  1.6
 */
class IssuesModelCat extends JModelItem
{
  public function getItem($id = null)
	{
    // echo 'here';
    $some_value = JRequest::getVar('id');;
    // var_dump($some_value);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id, title, description, extension');
		$query->from($db->quoteName('#__categories'));
		// $query->where($db->quoteName('extension')." = ".$db->quote($some_value));com_issues
		$query->where($db->quoteName('id')." = $some_value");

		$db->setQuery($query);
    // $result = $db->loadResult();
		// $results = $db->loadRowList();
    $row = $db->loadRow();

		return $row;
  }


  public function getChildren($id = null)
	{
    // echo 'here';
    $some_value = JRequest::getVar('id');
    // var_dump($some_value);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__issues'));
		// $query->where($db->quoteName('extension')." = ".$db->quote($some_value));com_issues
		$query->where($db->quoteName('category')." = $some_value");

		$db->setQuery($query);
    // $result = $db->loadResult();
		$results = $db->loadRowList();
    // $row = $db->loadRow();
    // print_r($results);

		return $results;
  }

  public function getCount($id = null)
  {
    $some_value = JRequest::getVar('id');
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select('COUNT(*)');
    $query->from($db->quoteName('#__issues'));
    $query->where($db->quoteName('category')." = $some_value");

    // Reset the query using our newly populated query object.
    $db->setQuery($query);
    $count = $db->loadResult();
    // var_dump($count);
  }
}
