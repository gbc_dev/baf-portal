<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

/*defined('_JEXEC') or die;

/**
 * Content Component Category Tree
 *
 * @since  1.6
 *
class IssuesCategories extends JCategories
{
  /**
   * Class constructor
   *
   * @param   array  $options  Array of options
   *
   * @since   11.1
   *
  public function __construct($options = array())
  {
    $options['table'] = '#__content';
    $options['extension'] = 'com_issues';

    parent::__construct($options);
  }
}*/

defined('_JEXEC') or die;

/**
 * ThirdParty Component Category Tree
 */
class IssuesCategories extends JCategories
{
    /**
     * Constructor
     *
     * @param   array  $options  Array of options
     */
    public function __construct($options = array())
    {
        $options['table']      = '#__issues';
        $options['extension']  = 'com_issues';
        $options['statefield'] = 'published';
        parent::__construct($options);
    }
}