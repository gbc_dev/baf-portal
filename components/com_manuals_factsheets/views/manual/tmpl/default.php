<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Manuals_factsheets
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$canEdit = JFactory::getUser()->authorise('core.edit', 'com_manuals_factsheets.' . $this->item->id);

if (!$canEdit && JFactory::getUser()->authorise('core.edit.own', 'com_manuals_factsheets' . $this->item->id))
{
	$canEdit = JFactory::getUser()->id == $this->item->created_by;
}

// var_dump($this->item);
?>

<div class="item_fields hidden">

	<table class="table">
		

		<tr>
			<th><?php echo JText::_('COM_MANUALS_FACTSHEETS_FORM_LBL_MANUAL_DOCUMENT_NAME'); ?></th>
			<td><?php echo $this->item->document_name; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_MANUALS_FACTSHEETS_FORM_LBL_MANUAL_NOTE'); ?></th>
			<td><?php echo nl2br($this->item->note); ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_MANUALS_FACTSHEETS_FORM_LBL_MANUAL_FILE_NAME'); ?></th>
			<td><?php echo $this->item->file_name; ?></td>
		</tr>

		<tr>
			<th><?php echo JText::_('COM_MANUALS_FACTSHEETS_FORM_LBL_MANUAL_FILE_UPLOAD'); ?></th>
			<td>
			<?php
			foreach ((array) $this->item->file_upload as $singleFile) : 
				if (!is_array($singleFile)) : 
					$uploadPath = 'docs' . DIRECTORY_SEPARATOR . $singleFile;
					 echo '<a href="' . JRoute::_(JUri::root() . $uploadPath, false) . '" target="_blank">' . $singleFile . '</a> ';
				endif;
			endforeach;
		?></td>
		</tr>

		<!--<tr>
			<th><?php //echo JText::_('COM_MANUALS_FACTSHEETS_FORM_LBL_MANUAL_PUBLISH_UP'); ?></th>
			<td><?php //echo $this->item->publish_up; ?></td>
		</tr>

		<!--<tr>
			<th><?php //echo JText::_('COM_MANUALS_FACTSHEETS_FORM_LBL_MANUAL_PUBLISH_DOWN'); ?></th>
			<td><?php //echo $this->item->publish_down; ?></td>
		</tr>

		 <tr>
			<th><?php //echo JText::_('COM_MANUALS_FACTSHEETS_FORM_LBL_MANUAL_ACCESS'); ?></th>
			<td><?php //echo $this->item->access; ?></td>
		</tr> -->

		<tr>
			<th><?php //echo JText::_('COM_MANUALS_FACTSHEETS_FORM_LBL_MANUAL_CATEGORY'); ?></th>
			<td><?php //echo $this->item->category; ?></td>
		</tr>

	</table>

</div>

<!-- CUstom view -->
<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 documents-sidebar hidden">
			<section class="widget">
        <div class="inner">
            <aside class="library-search">
    <h3>Search Manuals</h3>

    <form method="get" action="#" class="library-search-form">
        <div class="form-group">
            <div class="hint">Enter your search query below to search for documents by any field:</div>            <input type="search" class="form-control form-control-lg" name="fwp_search_library" required="">
        </div>

        <div class="form-group">
            <div class="hint">Select the categories you want to include in this search:</div>            <div class="checkbox">
                     <div class="inner">
                        <label class="facetwp-style-checkbox">
                            <input type="checkbox" name="select_document_categories[]" value="aegis-working-papers">
                            <span></span>
                            Draft Papers                        
                        </label>
                    </div>
                    <div class="inner">
                        <label class="facetwp-style-checkbox">
                            <input type="checkbox" name="select_document_categories[]" value="aegis-policy-briefs">
                            <span></span>
                            Aegis Policy Briefs                        
                        </label>
                    </div>
                                    <div class="inner">
                        <label class="facetwp-style-checkbox">
                            <input type="checkbox" name="select_document_categories[]" value="reports">
                            <span></span>
                            Reports                       
                             </label>
                    </div>
                    <div class="inner">
                        <label class="facetwp-style-checkbox">
                            <input type="checkbox" name="select_document_categories[]" value="newspaper-articles">
                            <span></span>
                            Newspaper Articles                        
                        </label>
                    </div>
                    <div class="inner">
                        <label class="facetwp-style-checkbox">
                            <input type="checkbox" name="select_document_categories[]" value="research-papers">
                            <span></span>
                            Research Papers                        
                        </label>
                    </div>
                     <div class="inner">
                        <label class="facetwp-style-checkbox">
                            <input type="checkbox" name="select_document_categories[]" value="books">
                            <span></span>
                            Books                        
                        </label>
                    </div>
                     <div class="inner">
                        <label class="facetwp-style-checkbox">
                            <input type="checkbox" name="select_document_categories[]" value="policy-briefs">
                            <span></span>
                            Policy Briefs                        </label>
                    </div>
                                    <div class="inner">
                        <label class="facetwp-style-checkbox">
                            <input type="checkbox" name="select_document_categories[]" value="policy-documents">
                            <span></span>
                            Policy Documents                        </label>
                    </div>
                                    <div class="inner">
                        <label class="facetwp-style-checkbox">
                            <input type="checkbox" name="select_document_categories[]" value="audiovisual">
                            <span></span>
                            Audiovisual                        </label>
                    </div>
                            </div>
        </div>

        <input type="hidden" name="fwp_document_categories" value="" disabled="disabled">

        <button class="btn btn-lg btn-primary" type="submit">
            <i class="fa fa-arrow-circle-o-right"></i> SEARCH
        </button>
    </form>
</aside>        </div>
    </section>
		</div>

		<div class="col-md-12">
			<article class="document">
				<div class="clearfix">
					<?php
					$previous = "javascript:history.go(-1)";
					if(isset($_SERVER['HTTP_REFERER'])) {
					    $previous = $_SERVER['HTTP_REFERER'];
					}
					?>
                <a class="back-to-category" href="<?= $previous ?>">
                    <i class="fa fa-arrow-circle-o-left"></i> Back to other <?php echo $this->item->category; ?>               </a>
            </div>

            <header>
		        <div class="research-type">Document type: <?php echo $this->item->category; ?></div>
		        <h1><?php echo $this->item->document_name; ?></h1>
		    </header>

		    <ul class="document-meta">
		                        <?php if(!empty($this->item->external_uri)){ ?>
                                <li>
                            <dl>
                                <dt>External URL</dt>
                                <dd> <a href="<?php echo $this->item->external_uri; ?>" target="_blank"><?php echo $this->item->external_uri; ?></a></dd>
                            </dl>
                        </li>
                        <?php } ?>
					                        <li>
                            <dl>
                                <dt>Publisher</dt>
                                <dd><a href="#" target="_blank"><?php echo $this->item->created_by_name; ?></a></dd>
                            </dl>
                        </li>
					    </ul>
<div class="clearfix"></div>
<?php
			foreach ((array) $this->item->file_upload as $singleFile) : 
				if (!is_array($singleFile)) : 
					$uploadPath = 'docs' . DIRECTORY_SEPARATOR . $singleFile;
					 echo '<a class="btn btn-primary btn-lg btn-download-document" href="' . JRoute::_(JUri::root() . $uploadPath, false) . '" target="_blank"><i class="fa fa-download"></i> Download this Document</a> ';
				endif;
			endforeach;
		?>
			<!-- <a class="btn btn-primary btn-lg btn-download-document" href="http://www.genocideresearchhub.org.rw/app/uploads/2017/09/Benda-WP001-Final.pdf" target="_blank" data-event-label="Youth Connekt Dialogue: Unwanted Legacies, Responsibility and Nation-building in Rwanda" data-event-value="1599">
                <i class="fa fa-download"></i> Download this Document
            </a> -->

            <div class="document">
            	<br/>
            	<?php echo nl2br($this->item->note);
                //echo JURI::root();
                 ?>
                <br />
                <?php //var_dump($this->item->file_upload);?>

<iframe src="http://docs.google.com/gview?url=http://dialogkenya.info/docs/<?php echo $this->item->file_upload;?>&embedded=true" style="width:100%; height:500px;" frameborder="0"></iframe>
            </div>
			</article>
		</div>
        <br />
        <?php $uri = JFactory::getURI(); 
                    $pageURL = $uri->toString(); 
                    // $pageURL = $uri->var->id; 
                    // echo '<pre>';
                    // var_dump($pageURL);
                    // echo '<pre/>';
                    $page_id = $this->item->document_name; ?>
                    <div id="disqus_thread"></div>
                        <script>
                        
                        var disqus_config = function () {
                        this.page.url = '<?php echo $pageURL; ?>';  
                        this.page.identifier = '<?php echo $page_id;?>'; 
                        this.page.title = '<?php echo $page_id;?>'; 
                        // console.log(this.page);
                        };

                        (function() { // DON'T EDIT BELOW THIS LINE
                        var d = document, s = d.createElement('script');
                        s.src = 'https://dialogkenya.disqus.com/embed.js';
                        s.setAttribute('data-timestamp', +new Date());
                        (d.head || d.body).appendChild(s);
                        })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by DialogKenya.</a></noscript>
	</div>
    
</div>
<!-- End custom view -->

<?php if($canEdit && $this->item->checked_out == 0): ?>

	<a class="btn" href="<?php echo JRoute::_('index.php?option=com_manuals_factsheets&task=manual.edit&id='.$this->item->id); ?>"><?php echo JText::_("COM_MANUALS_FACTSHEETS_EDIT_ITEM"); ?></a>

<?php endif; ?>

<?php if (JFactory::getUser()->authorise('core.delete','com_manuals_factsheets.manual.'.$this->item->id)) : ?>

	<a class="btn btn-danger" href="#deleteModal" role="button" data-toggle="modal">
		<?php echo JText::_("COM_MANUALS_FACTSHEETS_DELETE_ITEM"); ?>
	</a>

	<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><?php echo JText::_('COM_MANUALS_FACTSHEETS_DELETE_ITEM'); ?></h3>
		</div>
		<div class="modal-body">
			<p><?php echo JText::sprintf('COM_MANUALS_FACTSHEETS_DELETE_CONFIRM', $this->item->id); ?></p>
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal">Close</button>
			<a href="<?php echo JRoute::_('index.php?option=com_manuals_factsheets&task=manual.remove&id=' . $this->item->id, false, 2); ?>" class="btn btn-danger">
				<?php echo JText::_('COM_MANUALS_FACTSHEETS_DELETE_ITEM'); ?>
			</a>
		</div>
	</div>

<?php endif; ?>