<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Manuals_factsheets
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');
jimport('joomla.application.component.helper');

$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_manuals_factsheets') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'manualform.xml');
$canEdit    = $user->authorise('core.edit', 'com_manuals_factsheets') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'manualform.xml');
$canCheckin = $user->authorise('core.manage', 'com_manuals_factsheets');
$canChange  = $user->authorise('core.edit.state', 'com_manuals_factsheets');
$canDelete  = $user->authorise('core.delete', 'com_manuals_factsheets');

// var_dump($this);

    // $app = JFactory::getApplication();

    // $mycom_params =  & $app->getParams('com_content');


	$app = JFactory::getApplication('site');
	$componentParams = $app->getParams('com_manuals_factsheets');
	// $param = $componentParams->get('paramName', defaultValue);


    // var_dump($mycom_params);

    echo $componentParams->get('menu-meta_description');
?>
<hr>
<div class="col-md-9">
<form action="<?php echo htmlspecialchars(JUri::getInstance()->toString()); ?>" method="post"
      name="adminForm" id="adminForm">

	<?php echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__)); ?>
	<table class="table table-striped table-responsive" id="manualList">
		<thead>
		<tr>
			<?php if (isset($this->items[0]->state)): ?>
				<th class="hidden" width="5%">
	<?php echo JHtml::_('grid.sort', 'JPUBLISHED', 'a.state', $listDirn, $listOrder); ?>
</th>
			<?php endif; ?>

							<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_MANUALS_FACTSHEETS_MANUALS_ID', 'a.id', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_MANUALS_FACTSHEETS_MANUALS_CREATED_BY', 'a.created_by', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_MANUALS_FACTSHEETS_MANUALS_MODIFIED_BY', 'a.modified_by', $listDirn, $listOrder); ?>
				</th>
				<th class='' style="width:  33%;">
				<?php echo JHtml::_('grid.sort',  'Document', 'a.document_name', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_MANUALS_FACTSHEETS_MANUALS_NOTE', 'a.note', $listDirn, $listOrder); ?>
				</th>
				<th class=''>
				<?php echo JHtml::_('grid.sort',  'Description', 'a.brief', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'Download', 'a.file_upload', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_MANUALS_FACTSHEETS_MANUALS_PUBLISHED', 'a.published', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_MANUALS_FACTSHEETS_MANUALS_CREATED', 'a.created', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_MANUALS_FACTSHEETS_MANUALS_MODIFIED', 'a.modified', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_MANUALS_FACTSHEETS_MANUALS_PUBLISH_UP', 'a.publish_up', $listDirn, $listOrder); ?>
				</th>
				<th class='hidden'>
				<?php echo JHtml::_('grid.sort',  'COM_MANUALS_FACTSHEETS_MANUALS_PUBLISH_DOWN', 'a.publish_down', $listDirn, $listOrder); ?>
				</th>
				<th class='' style="width: 11%;">
				<?php echo JHtml::_('grid.sort',  'Category', 'a.category', $listDirn, $listOrder); ?>
				</th>


							<?php if ($canEdit || $canDelete): ?>
					<th class="center">
				<?php echo JText::_('COM_MANUALS_FACTSHEETS_MANUALS_ACTIONS'); ?>
				</th>
				<?php endif; ?>

		</tr>
		</thead>
		<tfoot>
		<tr>
			<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
		</tfoot>
		<tbody>
		<?php foreach ($this->items as $i => $item) : ?>
			<?php $canEdit = $user->authorise('core.edit', 'com_manuals_factsheets'); ?>

							<?php if (!$canEdit && $user->authorise('core.edit.own', 'com_manuals_factsheets')): ?>
					<?php $canEdit = JFactory::getUser()->id == $item->created_by; ?>
				<?php endif; ?>

			<tr class="row<?php echo $i % 2; ?>">

				<?php if (isset($this->items[0]->state)) : ?>
					<?php $class = ($canChange) ? 'active' : 'disabled'; ?>
					<td class="center hidden">
	<a class="btn btn-micro <?php echo $class; ?>" href="<?php echo ($canChange) ? JRoute::_('index.php?option=com_manuals_factsheets&task=manual.publish&id=' . $item->id . '&state=' . (($item->state + 1) % 2), false, 2) : '#'; ?>">
	<?php if ($item->state == 1): ?>
		<i class="icon-publish"></i>
	<?php else: ?>
		<i class="icon-unpublish"></i>
	<?php endif; ?>
	</a>
</td>
				<?php endif; ?>

				<td class="hidden">
					<?php echo $item->id; ?>
				</td>
				<td class="hidden">
							<?php echo JFactory::getUser($item->created_by)->name; ?>
				</td>
				<td class="hidden">
							<?php echo JFactory::getUser($item->modified_by)->name; ?>
				</td>
				<td>
				<?php if (isset($item->checked_out) && $item->checked_out) : ?>
					<?php //echo JHtml::_('jgrid.checkedout', $i, $item->uEditor, $item->checked_out_time, 'manuals.', $canCheckin); ?>
				<?php endif; ?>
				<a href="<?php echo JRoute::_('index.php?option=com_manuals_factsheets&view=manual&id='.(int) $item->id); ?>">
				<?php echo $this->escape($item->document_name); ?></a>
				</td>
				<td class="hidden">

					<?php echo $item->note; ?>
				</td>
				<td>
					<?php
          // var_dump($item->brief);
          if(strlen($item->brief) < 100){
            $name = $item->brief;
            echo $name;
          }else{
            $name = substr($item->brief,0,180). '';
            echo $name;
          }
           // echo $item->brief; ?>
				</td>
				<td class="hidden">

					<?php
						if (!empty($item->file_upload)) :
							$file_uploadArr = (array) explode(',', $item->file_upload);
							foreach ($file_uploadArr as $singleFile) :
								if (!is_array($singleFile)) :
									$uploadPath = 'docs' . DIRECTORY_SEPARATOR . $singleFile;
									echo '<a href="' . JRoute::_(JUri::root() . $uploadPath, false) . '" target="_blank" title="See the file_upload">' . $singleFile . '</a> ';
								endif;
							endforeach;
						else:
							echo $item->file_upload;
						endif; ?>				</td>
				<td class="hidden">

					<?php echo $item->published; ?>
				</td>
				<td class="hidden">

					<?php echo $item->created; ?>
				</td>
				<td class="hidden">

					<?php echo $item->modified; ?>
				</td>
				<td class="hidden">

					<?php echo $item->publish_up; ?>
				</td>
				<td class="hidden">

					<?php echo $item->publish_down; ?>
				</td>
				<td>

					<?php echo $item->category; ?>
				</td>


								<?php if ($canEdit || $canDelete): ?>
					<td class="center">
						<?php if ($canEdit): ?>
							<a href="<?php echo JRoute::_('index.php?option=com_manuals_factsheets&task=manualform.edit&id=' . $item->id, false, 2); ?>" class="btn btn-mini" type="button"><i class="icon-edit" ></i></a>
						<?php endif; ?>
						<?php if ($canDelete): ?>
							<a href="<?php echo JRoute::_('index.php?option=com_manuals_factsheets&task=manualform.remove&id=' . $item->id, false, 2); ?>" class="btn btn-mini delete-button" type="button"><i class="icon-trash" ></i></a>
						<?php endif; ?>
					</td>
				<?php endif; ?>

			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

	<?php if ($canCreate) : ?>
		<a href="<?php echo JRoute::_('index.php?option=com_manuals_factsheets&task=manualform.edit&id=0', false, 0); ?>"
		   class="btn btn-success btn-small"><i
				class="icon-plus"></i>
			<?php echo JText::_('COM_MANUALS_FACTSHEETS_ADD_ITEM'); ?></a>
	<?php endif; ?>

	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
	<?php echo JHtml::_('form.token'); ?>
</form>

<?php if($canDelete) : ?>
<script type="text/javascript">

	jQuery(document).ready(function () {
		jQuery('.delete-button').click(deleteItem);
	});

	function deleteItem() {

		if (!confirm("<?php echo JText::_('COM_MANUALS_FACTSHEETS_DELETE_MESSAGE'); ?>")) {
			return false;
		}
	}
</script>
<?php endif; ?>
</div>
<div class="col-md-3">
	<?php
	  $position = 'manuals'; //position of module in the backend
	  $modules =& JModuleHelper::getModules($position);
	  if (!empty($modules)) { ?>
	    <div class="ibox float-e-margins">
	      <?php
	      foreach ($modules as $module) {
	        echo JModuleHelper::renderModule($module);
	      }
	       ?>
	       <div class="clearfix"></div>
	   </div>
	  <?php  }?>
	  <!-- <br /> -->
	  <?php /*
	  $position = 'publications'; //position of module in the backend
	  $modules =& JModuleHelper::getModules($position);
	  if (!empty($modules)) { ?>
	    <div class="ibox float-e-margins">
	<br />
	      <?php
	      foreach ($modules as $module) {
	        echo JModuleHelper::renderModule($module);
	      }
	       ?>
	       <div class="clearfix"></div>
	       <br/>
	       </br/>
	   </div>
	  <?php * }*/?>
	</div>
