<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Manuals_factsheets
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');

// Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_manuals_factsheets', JPATH_SITE);
$doc = JFactory::getDocument();
$doc->addScript(JUri::base() . '/media/com_manuals_factsheets/js/form.js');

$user    = JFactory::getUser();
$canEdit = Manuals_factsheetsHelpersManuals_factsheets::canUserEdit($this->item, $user);


?>

<div class="manual-edit front-end-edit">
	<?php if (!$canEdit) : ?>
		<h3>
			<?php throw new Exception(JText::_('COM_MANUALS_FACTSHEETS_ERROR_MESSAGE_NOT_AUTHORISED'), 403); ?>
		</h3>
	<?php else : ?>
		<?php if (!empty($this->item->id)): ?>
			<h1><?php echo JText::sprintf('COM_MANUALS_FACTSHEETS_EDIT_ITEM_TITLE', $this->item->id); ?></h1>
		<?php else: ?>
			<h1><?php echo JText::_('COM_MANUALS_FACTSHEETS_ADD_ITEM_TITLE'); ?></h1>
		<?php endif; ?>

		<form id="form-manual"
			  action="<?php echo JRoute::_('index.php?option=com_manuals_factsheets&task=manual.save'); ?>"
			  method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
			
	<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />

	<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />

	<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />

	<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />

	<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php echo $this->form->getInput('created_by'); ?>
				<?php echo $this->form->getInput('modified_by'); ?>
	<?php echo $this->form->renderField('document_name'); ?>

	<?php echo $this->form->renderField('note'); ?>

	<?php echo $this->form->renderField('file_name'); ?>

	<?php echo $this->form->renderField('file_upload'); ?>

				<?php if (!empty($this->item->file_upload)) : ?>
					<?php $file_uploadFiles = array(); ?>
					<?php foreach ((array)$this->item->file_upload as $fileSingle) : ?>
						<?php if (!is_array($fileSingle)) : ?>
							<a href="<?php echo JRoute::_(JUri::root() . 'docs' . DIRECTORY_SEPARATOR . $fileSingle, false);?>"><?php echo $fileSingle; ?></a> | 
							<?php $file_uploadFiles[] = $fileSingle; ?>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>
				<input type="hidden" name="jform[file_upload_hidden]" id="jform_file_upload_hidden" value="<?php echo implode(',', $file_uploadFiles); ?>" />
	<input type="hidden" name="jform[published]" value="<?php echo $this->item->published; ?>" />

	<input type="hidden" name="jform[created]" value="<?php echo $this->item->created; ?>" />

	<input type="hidden" name="jform[modified]" value="<?php echo $this->item->modified; ?>" />

	<?php echo $this->form->renderField('external_uri'); ?>

	<?php echo $this->form->renderField('publish_up'); ?>

	<?php echo $this->form->renderField('publish_down'); ?>

	<?php echo $this->form->renderField('access'); ?>

	<?php echo $this->form->renderField('category'); ?>
				<div class="fltlft hidden" <?php if (!JFactory::getUser()->authorise('core.admin','manuals_factsheets')): ?> style="display:none;" <?php endif; ?> >
                <?php echo JHtml::_('sliders.start', 'permissions-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
                <?php echo JHtml::_('sliders.panel', JText::_('ACL Configuration'), 'access-rules'); ?>
                <fieldset class="panelform">
                    <?php echo $this->form->getLabel('rules'); ?>
                    <?php echo $this->form->getInput('rules'); ?>
                </fieldset>
                <?php echo JHtml::_('sliders.end'); ?>
            </div>
				<?php if (!JFactory::getUser()->authorise('core.admin','manuals_factsheets')): ?>
                <script type="text/javascript">
                    jQuery.noConflict();
                    jQuery('.tab-pane select').each(function(){
                       var option_selected = jQuery(this).find(':selected');
                       var input = document.createElement("input");
                       input.setAttribute("type", "hidden");
                       input.setAttribute("name", jQuery(this).attr('name'));
                       input.setAttribute("value", option_selected.val());
                       document.getElementById("form-manual").appendChild(input);
                    });
                </script>
             <?php endif; ?>
			<div class="control-group">
				<div class="controls">

					<?php if ($this->canSave): ?>
						<button type="submit" class="validate btn btn-primary">
							<?php echo JText::_('JSUBMIT'); ?>
						</button>
					<?php endif; ?>
					<a class="btn"
					   href="<?php echo JRoute::_('index.php?option=com_manuals_factsheets&task=manualform.cancel'); ?>"
					   title="<?php echo JText::_('JCANCEL'); ?>">
						<?php echo JText::_('JCANCEL'); ?>
					</a>
				</div>
			</div>

			<input type="hidden" name="option" value="com_manuals_factsheets"/>
			<input type="hidden" name="task"
				   value="manualform.save"/>
			<?php echo JHtml::_('form.token'); ?>
		</form>
	<?php endif; ?>
</div>
