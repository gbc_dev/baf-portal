<?php
/**
 * @version    CVS: 1.0.1
 * @package    Com_Manuals_factsheets
 * @author     Michael Buluma <michael@buluma.me.ke>
 * @copyright  2017 Michael Buluma
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Manuals_factsheets', JPATH_COMPONENT);
JLoader::register('Manuals_factsheetsController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Manuals_factsheets');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
