<?php
/**
 * Kunena Component
 * @package     Kunena.Template.Crypsis
 * @subpackage  Layout.Topic
 *
 * @copyright   (C) 2008 - 2017 Kunena Team. All rights reserved.
 * @license     https://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/
defined('_JEXEC') or die;

// @var KunenaForumTopic $topic

$topic = $this->topic;
// $attachments = $this->attachments;
// $results = KunenaAttachmentHelper::getById($ids);
// $reults3 = $this->attachments;
$me = KunenaUserHelper::getMyself();

$message = $this->message;
$attachments = $message->getAttachments();

// echo '<pre>';
// var_dump($topic);
// echo '<pre/>';

// $a = $topic->first_post_message;

// if (strpos($a, '[url]') !== false) {
//     echo 'true';
// }


// $str = 'foo {url}123456789{/url} bar';
// preg_match('~{url}([^{]*){/url}~i', $str, $match);
// var_dump($match[1]); // string(9) "123456789"

// $str = $a;
// $m = substr($str, strpos($str, '[url]')+5);
// $m = substr($m, 0, strpos($m, '[/url]'));
// var_dump($m); // string(9) "123456789"


// function to collect url string

function getTagValues($tag, $str) {
    $re = sprintf("/\[(%s)\](.+?)\[\/\\1\]/", preg_quote($tag));
    preg_match_all($re, $str, $matches);
    return $matches[2];
}

// function get_string_between($string, $start, $end){
//     $string = ' ' . $string;
//     $ini = strpos($string, $start);
//     if ($ini == 0) return '';
//     $ini += strlen($start);
//     $len = strpos($string, $end, $ini) - $ini;
//     return substr($string, $ini, $len);
// }

// $fullstring = 'this is my [url]dogx[/tag]';
$fullstring = $topic->first_post_message;
$parsed = get_string_between($fullstring, ']', '[/url');

// echo $parsed; // (result = dog)

// $str = "[Vimeo]123456789[/Vimeo] and [Vimeo]123[/Vimeo]";
$str = $topic->first_post_message;


$my_string = "I am with a [id]123[/id] and [id]456[/id]";
preg_match_all("~\[id\](.*?)\[\/id\]~",$my_string,$m);
// print_r($m[1]);

function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

// $fullstring = 'I am with a [id]123[/id] and [id]456[id]';
// $parsed = get_string_between($fullstring, '[id]', '[/id]');

// $string = 'This is a [url=index.php?option=com_content&view=article&id=314&Itemid=19]link[/url]';
// // $string = 'reply-234-private';
// if (preg_match('/[url=(.*?)]-(.*?)-[/url]/', $string, $display) === 1) {
//     // echo $display[1];
// }


$this->addScriptDeclaration(
	'// <![CDATA[
var kunena_anonymous_name = "' . JText::_('COM_KUNENA_USERNAME_ANONYMOUS') . '";
// ]]>');

JText::script('COM_KUNENA_RATE_LOGIN');
JText::script('COM_KUNENA_RATE_NOT_YOURSELF');
JText::script('COM_KUNENA_RATE_ALLREADY');
JText::script('COM_KUNENA_RATE_SUCCESSFULLY_SAVED');

JText::script('COM_KUNENA_SOCIAL_EMAIL_LABEL');
JText::script('COM_KUNENA_SOCIAL_TWITTER_LABEL');
JText::script('COM_KUNENA_SOCIAL_FACEBOOK_LABEL');
JText::script('COM_KUNENA_SOCIAL_GOOGLEPLUS_LABEL');
JText::script('COM_KUNENA_SOCIAL_LINKEDIN_LABEL');
JText::script('COM_KUNENA_SOCIAL_PINTEREST_LABEL');
JText::script('COM_KUNENA_SOCIAL_STUMBLEUPON_LABEL');
JText::script('COM_KUNENA_SOCIAL_WHATSAPP_LABEL');

$this->addStyleSheet('assets/css/jquery.atwho.css');

// Load caret.js always before atwho.js script and use it for autocomplete, emojiis...
$this->addScript('assets/js/jquery.caret.js');
$this->addScript('assets/js/jquery.atwho.js');
$this->addScript('assets/js/topic.js');

$this->addStyleSheet('assets/css/rating.css');
$this->addScript('assets/js/rating.js');
$this->addScript('assets/js/krating.js');

$this->ktemplate = KunenaFactory::getTemplate();
$social = $this->ktemplate->params->get('socialshare');
$quick = $this->ktemplate->params->get('quick');
?>

<div class="col-md-9">
<?php //echo 'here';?>
<div><?php echo $this->subLayout('Widget/Module')->set('position', 'kunena_topic_top'); ?></div>
<?php /*if ($this->category->headerdesc) : ?>
<div class="alert alert-info">
	<a class="close" data-dismiss="alert" href="#">&times;</a>
	<?php echo $this->category->displayField('headerdesc'); ?>
</div>
<?php endif; */?>

<h1>
	<?php //echo $topic->getIcon($topic->getCategory()->iconset);?><!--  Hide category icon -->
	<?php
	if ($this->ktemplate->params->get('labels') != 0)
	{
		echo $this->subLayout('Widget/Label')->set('topic', $this->topic)->setLayout('default');
	}
	?>
	<?php echo $topic->displayField('subject');?>
	<?php echo $this->subLayout('Topic/Item/Rating')->set('category', $this->category)->set('topicid', $topic->id)->set('config', $this->config);?>
</h1>

<div><?php echo $this->subRequest('Topic/Item/Actions')->set('id', $topic->id); ?></div>

<?/*<div class="pull-left">
	<?php echo $this->subLayout('Widget/Pagination/List')
	->set('pagination', $this->pagination)
	->set('display', true); ?>
</div>

<h2 class="pull-right">
	<?php echo $this->subLayout('Widget/Search')
	->set('id', $topic->id)
	->set('title', JText::_('COM_KUNENA_SEARCH_TOPIC'))
	->setLayout('topic'); ?>
</h2>*/?>

<div class="clearfix"></div>

<?php
if ($this->ktemplate->params->get('displayModule'))
{
	echo $this->subLayout('Widget/Module')->set('position', 'kunena_topictitle');
}

echo $this->subRequest('Topic/Poll')->set('id', $topic->id);

if ($this->ktemplate->params->get('displayModule'))
{
	echo $this->subLayout('Widget/Module')->set('position', 'kunena_poll');
}

$count = 1;
foreach ($this->messages as $id => $message)
{
	echo $this->subRequest('Topic/Item/Message')
		->set('mesid', $message->id)
		->set('location', $id);

	if ($this->ktemplate->params->get('displayModule'))
	{
		echo $this->subLayout('Widget/Module')
			->set('position', 'kunena_msg_row_' . $count++);
	}
}

if ($quick == 2)
{
	echo $this->subLayout('Message/Edit')
		->set('message', $this->message)
		->setLayout('full');
}
?>

<div class="clearfix"></div>
<?php if ($social == 1) : ?>
        <div><?php echo $this->subLayout('Widget/Social'); ?></div>
<?php endif; ?>

<?php if ($social == 2) : ?>
        <div><?php echo $this->subLayout('Widget/Socialcustomtag'); ?></div>
<?php endif; ?>
<div class="clearfix"></div>

<br />
<div class="pull-left hidden">
	<?php echo $this->subLayout('Widget/Pagination/List')
	->set('pagination', $this->pagination)
	->set('display', true); ?>
</div>

<div class="pull-right hidden">
	<?php echo $this->subLayout('Widget/Search')
	->set('id', $topic->id)
	->set('title', JText::_('COM_KUNENA_SEARCH_TOPIC'))
	->setLayout('topic'); ?>
</div>

<div><?php //echo $this->subRequest('Topic/Item/Actions')->set('id', $topic->id); ?></div>

<?php if ($this->ktemplate->params->get('writeaccess')) : ?>
<div><?php echo $this->subLayout('Widget/Writeaccess')->set('id', $topic->id); ?></div>
<?php endif;


/*if ($this->config->enableforumjump)
{
	echo $this->subLayout('Widget/Forumjump')->set('categorylist', $this->categorylist);
}*/?>

<div class="pull-right"><?php echo $this->subLayout('Category/Moderators')->set('moderators', $this->category->getModerators(false)); ?></div>

<div class="clearfix"></div>

<?php

	$text='<p>Hello <a href="test.php">Link1</a> this is some <a href="bla.html">Link 2</a>, etc, etc</p>';
preg_match_all('~<a(.*?)href="([^"]+)"(.*?)>~', $text, $matches);

// var_dump($matches[2]);

/*
array
  0 => string 'test.php' (length=8)
  1 => string 'bla.html' (length=8)
*/




// $hitext = $message->displayField('message');
//   $doc = new DOMDocument();
// $doc->loadHTML($hitext);
// $anchors = $doc->getElementsByTagName('a');
// foreach($anchors as $node) {
//     echo $node->textContent;
//     if ($node->hasAttributes()) {
//         foreach($node->attributes as $a) {
//         	// var_dump($a);
//             echo ' | ' .$a->name.': '.$a->value;
//            // echo ' | ' .$a->name;
//         }
//     }
// }

$contetxt = $topic->first_post_message;

//var_dump($contetxt);
// echo '<pre>';
// var_dump($hitext);
// var_dump($text);
// echo '<pre/>';
?>
<!-- <h3>A demonstration of how to access a SMALL element</h3>

<p>Click the button to set the color of <small id="mySmall">this small text</small> to red.</p>

<button onclick="myFunction()">Try it</button>

<script>
function myFunction() {
    var x = document.getElementById("kmsg");
    x.style.color= "red";
}
</script> -->


<?php /*if ($this->category->headerdesc) : ?>
<div class="alert alert-info">
	<a class="close" data-dismiss="alert" href="#">&times;</a>
	<?php echo $this->category->displayField('headerdesc'); ?>
</div>
<?php endif; */?>

<?php if ($this->category->headerdesc) : ?>
<div class="cat_Desc">
	<div class="col-md-1">
	<i class="fa fa-folder-open-o fa-6" aria-hidden="true"></i>
	</div>
	<div class="col-md-11">
		<?php echo $this->category->displayField('headerdesc'); ?>
	</div>
</div>
<?php endif; ?>

</div>

<div class="col-md-3">
	<?php 
	// var_dump(getTagValues("url", $str)); 
	$issueLinks = getTagValues("url", $str);

	// $issueLinks = $parsed;
	// var_dump($issueLinks);
	// $html = file_get_contents('bookmarks.html');
	$html = $text;
	$newtext = $message->displayField('message');
	//Create a new DOM document
	$dom = new DOMDocument;

	//Parse the HTML. The @ is used to suppress any parsing errors
	//that will be thrown if the $html string isn't valid XHTML.
	@$dom->loadHTML($newtext);

	//Get all links. You could also use any other tag name here,
	//like 'img' or 'table', to extract other tags.
	$links = $dom->getElementsByTagName('a');
	// $players = $dom->getElementsByClassName('kmsgtext-quote');
	// $players = document.getElementsByClassName(classname);


	//Iterate over the extracted links and display their URLs
	// foreach ($links as $link){
		// echo '<pre>';
		// var_dump($players);
		// var_dump($newtext);
		// echo '<pre/>';
	//     //Extract and show the "href" attribute.
	//     // echo $link->nodeValue;
	//     // echo ' '. $link->getAttribute('href');
	// } 

	// if (($links >= '1') && $issueLinks <= '1' { 
	if (!empty($links)) {
		?>
		<div class="issueLinks">
				<h2><i class="fa fa-link" aria-hidden="true"></i> &nbsp;Issue Links</h2>
				<ul>
					<?php foreach ($links as $link) {
						// var_dump($link);
					 ?>
						<li><i class="fa fa-link" aria-hidden="true"></i> <a href="<?php echo $link->getAttribute('href'); ?>" target="_blank"><?php echo $link->textContent;?></a> </li>
					<?php }  ?>
				</ul>
			</div>
			<div class="clearfix"></div>
	<?php } else{
		echo 'nothing to show';
	}
	?>
	
	<?php if (!empty($attachments)) : ?>
		<br />
	<div class="issueDocs">
		<h2><i class="fa fa-folder" aria-hidden="true"></i> &nbsp;Issue Documents</h2>
		<?php //$attachment = $this->attachment;
		// ?var_dump($attachments);
		 ?>
	<div class="kattach">
		<!-- <h5> <?php //echo JText::_('COM_KUNENA_ATTACHMENTS'); ?> </h5> -->
		<ul class="">
			<?php foreach ($attachments as $attachment) : ?>
				<li><a href="index.php?option=com_kunena&view=attachment&id=<?php echo $attachment->id;?>&format=raw&Itemid=649" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?php echo $attachment->filename_real; ?></a></li>
				<!-- <li class="span3 center">
					<div class="thumbnail"> <?php //echo $attachment->getLayout()->render('thumbnail'); ?> <?php //echo $attachment->getLayout()->render('textlink'); ?> </div>
				</li> -->
			<?php endforeach; ?>
		</ul>
	</div>
		<!-- <ul>
			<li><i class="fa fa-folder" aria-hidden="true"></i> One</li>
			<li><i class="fa fa-file-pdf-o" aria-hidden="true"></i> second</li>
			<li><i class="fa fa-file-text-o" aria-hidden="true"></i> three</li>
		</ul> -->

		<?php elseif ($attachs->total > 0  && !$this->me->exists()) :
			if ($attachs->image > 0 && !$this->config->showimgforguest)
			{
				if ($attachs->image > 1)
				{
					echo KunenaLayout::factory('BBCode/Image')->set('title', JText::_('COM_KUNENA_SHOWIMGFORGUEST_HIDEIMG_MULTIPLES'))->setLayout('unauthorised');
				}
				else
				{
					echo KunenaLayout::factory('BBCode/Image')->set('title', JText::_('COM_KUNENA_SHOWIMGFORGUEST_HIDEIMG_SIMPLE'))->setLayout('unauthorised');
				}
			}

			if ($attachs->file > 0 && !$this->config->showfileforguest)
			{
				if ($attachs->file > 1)
				{
					echo KunenaLayout::factory('BBCode/Image')->set('title', JText::_('COM_KUNENA_SHOWIMGFORGUEST_HIDEFILE_MULTIPLES'))->setLayout('unauthorised');
				}
				else
				{
					echo KunenaLayout::factory('BBCode/Image')->set('title', JText::_('COM_KUNENA_SHOWIMGFORGUEST_HIDEFILE_SIMPLE'))->setLayout('unauthorised');
				}
			}
			?>
			</div>
		<?php endif;
		 ?>
	<div class="clearfix"></div>
	<br />

	<div class="issueDocs">
		<h2><i class="fa fa-user" aria-hidden="true"></i> &nbsp;Key Players</h2>
		<ul>
		<?php
// 		$doc = new DOMDocument(); we'll come back to this
// $doc->loadHTML("<html><body>Test<br></body></html>");
// echo $doc->saveHTML();

		preg_match_all('/<div class="kmsgtext-quote" id="msg-quote">(.*?)<\/div>/s', $newtext, $matches);
		// print_r($matches);
		if (empty($matches)){
			echo 'no players found';
		}else{
		for ($i = 0; $i < count($matches[1]); $i++) {

       //here you can implement an if/else to check if the ID exist ?>
       <li><i class="fa fa-user" aria-hidden="true"></i> 
        <?php echo $matches[1][$i];?>
    	</li>
    	<?php }
		}
		?>
		</ul>
	</div>


</div>

<!-- <script>
// 	document.addEventListener('DOMContentLoaded', function() {
//     alert("Ready!");
// }, false);

function myFunction() {
    var div = document.getElementById("kmsg");
    var nodelist = div.getElementsByTagName("div");

    console.log(nodelist);

    
    var i;
    for (i = 0; i < nodelist.length; i++) {
        nodelist[i].style.backgroundColor = "red";

    }
}

</script> -->